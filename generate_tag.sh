#!/bin/bash

# retrieve last lightweight tag's name
_last_tag=$(git describe --abbrev=0 --tags)
echo "Last tag's name was: $_last_tag"
# retrieve last version's major value
_last_major=$(echo $_last_tag | cut -d'.' -f 1)
echo "Last tag's major value was: $_last_major"

# set new version's major value
_new_major=$(($_last_major+1))
echo "New major value: $_new_major"

# store current date into _date
printf -v _date '%(%Y%m%d)T' -1
echo "Current date: $_date"

# create new version number
_new_version="$_new_major.0.0+$_date"
echo "New version number: $_new_version"

# write new version number into newtag.env
echo "NEW_TAG=$_new_version" > new_tag.env
