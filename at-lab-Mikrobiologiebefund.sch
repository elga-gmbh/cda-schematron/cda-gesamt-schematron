<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Mikrobiologiebefund v3 (1.2.40.0.34.777.11.4.8 2023-05-09T07:02:45)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:hl7-org:sdtc" prefix="sdtc"/>
   <ns uri="urn:hl7-at:v3" prefix="hl7at"/>
   <ns uri="urn:hl7-org:ips" prefix="ips"/>
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <!-- Include realm specific schematron -->
   <!-- Include scenario label -->
   <let name="scenariolabel" value="'Mikrobiologiebefund'"/>
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario Mikrobiologiebefund -->

   <!-- atlab_document_Mikrobiologiebefund -->
   <pattern>
      <title>atlab_document_Mikrobiologiebefund</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.4.9.3'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.14']]"
                 see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.0.14-2021-03-29T152459.html">(atlab_document_Mikrobiologiebefund): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.4.9.3'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.14']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.6.0.11.0.14-2021-03-29T152459.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.0.14-2021-03-29T152459-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.14-2021-03-29T152459"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.106-2021-04-06T084653"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.107-2021-04-06T085654"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.110-2021-01-22T111158"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.111-2021-01-18T143448"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.112-2021-08-11T101637"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.114-2021-08-11T103917"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.15-2021-01-14T121839"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.6-2021-01-14T095840"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.12-2021-01-20T123601"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.160-2021-01-18T125824"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.166-2021-02-02T104626"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.25-2023-06-01T135303"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.26-2023-06-01T135016"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.27-2023-06-01T135820"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atlab_document_Mikrobiologiebefund">
      <active pattern="template-1.2.40.0.34.6.0.11.0.14-2021-03-29T152459"/>
   </phase>
   <phase id="atlab_document_Mikrobiologiebefund-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.14-2021-03-29T152459-closed"/>
   </phase>
   <phase id="atcdabbr_section_Befundbewertung">
      <active pattern="template-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636"/>
   </phase>
   <phase id="atcdabbr_section_Befundbewertung-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.103-2021-02-03T095636-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMikroskopieCodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMikroskopieCodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionKulturellerErregernachweis">
      <active pattern="template-1.2.40.0.34.6.0.11.2.106-2021-04-06T084653"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionKulturellerErregernachweis-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.106-2021-04-06T084653-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMolekularerErregernachweis">
      <active pattern="template-1.2.40.0.34.6.0.11.2.107-2021-04-06T085654"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMolekularerErregernachweis-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.107-2021-04-06T085654-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionInfektionsserologie">
      <active pattern="template-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionInfektionsserologie-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638-closed"/>
   </phase>
   <phase id="atlab_section_AnamneseLaborUndMikrobiologieCodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709"/>
   </phase>
   <phase id="atlab_section_AnamneseLaborUndMikrobiologieCodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.109-2021-05-05T081709-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionWeitereAnalysen">
      <active pattern="template-1.2.40.0.34.6.0.11.2.110-2021-01-22T111158"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionWeitereAnalysen-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.110-2021-01-22T111158-closed"/>
   </phase>
   <phase id="atlab_section_AnamneseLaborUndMikrobiologieUncodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.111-2021-01-18T143448"/>
   </phase>
   <phase id="atlab_section_AnamneseLaborUndMikrobiologieUncodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.111-2021-01-18T143448-closed"/>
   </phase>
   <phase id="atlab_section_AngeforderteUntersuchungenUncodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.112-2021-08-11T101637"/>
   </phase>
   <phase id="atlab_section_AngeforderteUntersuchungenUncodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.112-2021-08-11T101637-closed"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602"/>
   </phase>
   <phase id="atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602-closed"/>
   </phase>
   <phase id="atlab_section_UeberweisungsgrundUncodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.114-2021-08-11T103917"/>
   </phase>
   <phase id="atlab_section_UeberweisungsgrundUncodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.114-2021-08-11T103917-closed"/>
   </phase>
   <phase id="atlab_section_AngeforderteUntersuchungenCodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.15-2021-01-14T121839"/>
   </phase>
   <phase id="atlab_section_AngeforderteUntersuchungenCodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.15-2021-01-14T121839-closed"/>
   </phase>
   <phase id="atlab_section_UeberweisungsgrundCodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.6-2021-01-14T095840"/>
   </phase>
   <phase id="atlab_section_UeberweisungsgrundCodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.6-2021-01-14T095840-closed"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed"/>
   </phase>
   <phase id="atlab_section_ProbeninformationSpecimenSection">
      <active pattern="template-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118"/>
   </phase>
   <phase id="atlab_section_ProbeninformationSpecimenSection-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.93-2021-01-14T140118-closed"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed"/>
   </phase>
   <phase id="atlab_entry_AnamneseEntryLaborUndMikrobiologie">
      <active pattern="template-1.2.40.0.34.6.0.11.3.12-2021-01-20T123601"/>
   </phase>
   <phase id="atlab_entry_AnamneseEntryLaborUndMikrobiologie-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.12-2021-01-20T123601-closed"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed"/>
   </phase>
   <phase id="atlab_entry_ProbeninformationSpecimenEntry">
      <active pattern="template-1.2.40.0.34.6.0.11.3.160-2021-01-18T125824"/>
   </phase>
   <phase id="atlab_entry_ProbeninformationSpecimenEntry-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.160-2021-01-18T125824-closed"/>
   </phase>
   <phase id="atlab_entry_AnamneseObservationLaborUndMikrobiologie">
      <active pattern="template-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609"/>
   </phase>
   <phase id="atlab_entry_AnamneseObservationLaborUndMikrobiologie-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.164-2021-05-05T101609-closed"/>
   </phase>
   <phase id="atcdabbr_entry_NotificationOrganizer">
      <active pattern="template-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249"/>
   </phase>
   <phase id="atcdabbr_entry_NotificationOrganizer-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249-closed"/>
   </phase>
   <phase id="atcdabbr_entry_NotifiableCondition">
      <active pattern="template-1.2.40.0.34.6.0.11.3.166-2021-02-02T104626"/>
   </phase>
   <phase id="atcdabbr_entry_NotifiableCondition-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.166-2021-02-02T104626-closed"/>
   </phase>
   <phase id="atlab_entry_AngeforderteUntersuchungEntry">
      <active pattern="template-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708"/>
   </phase>
   <phase id="atlab_entry_AngeforderteUntersuchungEntry-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.169-2021-05-05T145708-closed"/>
   </phase>
   <phase id="atcdabbr_entry_caseIdentification">
      <active pattern="template-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713"/>
   </phase>
   <phase id="atcdabbr_entry_caseIdentification-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.170-2020-09-29T112713-closed"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryReportDataProcessing-20230601T135303">
      <active pattern="template-1.2.40.0.34.6.0.11.3.25-2023-06-01T135303"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryReportDataProcessing-20230601T135303-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.25-2023-06-01T135303-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryBatteryOrganizer-20230601T135016">
      <active pattern="template-1.2.40.0.34.6.0.11.3.26-2023-06-01T135016"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryBatteryOrganizer-20230601T135016-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.26-2023-06-01T135016-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryObservation-20230601T135820">
      <active pattern="template-1.2.40.0.34.6.0.11.3.27-2023-06-01T135820"/>
   </phase>
   <phase id="atcdabbr_entry_LaboratoryObservation-20230601T135820-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.27-2023-06-01T135820-closed"/>
   </phase>
   <phase id="elgagab_entry_Konsultationsgrund-20210219T104636">
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636"/>
   </phase>
   <phase id="elgagab_entry_Konsultationsgrund-20210219T104636-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.30-2021-02-19T104636-closed"/>
   </phase>
   <phase id="elgagab_entry_KonsultationsgrundProblem-20230202T154005">
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005"/>
   </phase>
   <phase id="elgagab_entry_KonsultationsgrundProblem-20230202T154005-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.31-2023-02-02T154005-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- atcdabbr_section_Befundbewertung -->
   <include href="include/1.2.40.0.34.6.0.11.2.103-2021-02-03T095636.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.103-2021-02-03T095636-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionMikroskopieCodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.105-2021-04-06T081032-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionKulturellerErregernachweis -->
   <include href="include/1.2.40.0.34.6.0.11.2.106-2021-04-06T084653.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.106-2021-04-06T084653-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionMolekularerErregernachweis -->
   <include href="include/1.2.40.0.34.6.0.11.2.107-2021-04-06T085654.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.107-2021-04-06T085654-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionInfektionsserologie -->
   <include href="include/1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.108-2021-04-06T094638-closed.sch"/>
   <!-- atlab_section_AnamneseLaborUndMikrobiologieCodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.109-2021-05-05T081709.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.109-2021-05-05T081709-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionWeitereAnalysen -->
   <include href="include/1.2.40.0.34.6.0.11.2.110-2021-01-22T111158.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.110-2021-01-22T111158-closed.sch"/>
   <!-- atlab_section_AnamneseLaborUndMikrobiologieUncodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.111-2021-01-18T143448.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.111-2021-01-18T143448-closed.sch"/>
   <!-- atlab_section_AngeforderteUntersuchungenUncodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.112-2021-08-11T101637.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.112-2021-08-11T101637-closed.sch"/>
   <!-- atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.113-2021-08-11T102602-closed.sch"/>
   <!-- atlab_section_UeberweisungsgrundUncodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.114-2021-08-11T103917.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.114-2021-08-11T103917-closed.sch"/>
   <!-- atlab_section_AngeforderteUntersuchungenCodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.15-2021-01-14T121839.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.15-2021-01-14T121839-closed.sch"/>
   <!-- atlab_section_UeberweisungsgrundCodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.6-2021-01-14T095840.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.6-2021-01-14T095840-closed.sch"/>
   <!-- atcdabbr_section_Brieftext -->
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed.sch"/>
   <!-- atcdabbr_section_AbschliessendeBemerkung -->
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed.sch"/>
   <!-- atcdabbr_section_Beilagen -->
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed.sch"/>
   <!-- atcdabbr_section_Uebersetzung -->
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed.sch"/>
   <!-- atlab_section_ProbeninformationSpecimenSection -->
   <include href="include/1.2.40.0.34.6.0.11.2.93-2021-01-14T140118.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.93-2021-01-14T140118-closed.sch"/>
   <!-- atcdabrr_entry_Comment -->
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed.sch"/>
   <!-- atlab_entry_AnamneseEntryLaborUndMikrobiologie -->
   <include href="include/1.2.40.0.34.6.0.11.3.12-2021-01-20T123601.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.12-2021-01-20T123601-closed.sch"/>
   <!-- atcdabbr_entry_externalDocument -->
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed.sch"/>
   <!-- atlab_entry_ProbeninformationSpecimenEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.160-2021-01-18T125824.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.160-2021-01-18T125824-closed.sch"/>
   <!-- atlab_entry_AnamneseObservationLaborUndMikrobiologie -->
   <include href="include/1.2.40.0.34.6.0.11.3.164-2021-05-05T101609.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.164-2021-05-05T101609-closed.sch"/>
   <!-- atcdabbr_entry_NotificationOrganizer -->
   <include href="include/1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.165-2021-02-02T084249-closed.sch"/>
   <!-- atcdabbr_entry_NotifiableCondition -->
   <include href="include/1.2.40.0.34.6.0.11.3.166-2021-02-02T104626.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.166-2021-02-02T104626-closed.sch"/>
   <!-- atlab_entry_AngeforderteUntersuchungEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.169-2021-05-05T145708.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.169-2021-05-05T145708-closed.sch"/>
   <!-- atcdabbr_entry_caseIdentification -->
   <include href="include/1.2.40.0.34.6.0.11.3.170-2020-09-29T112713.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.170-2020-09-29T112713-closed.sch"/>
   <!-- atcdabbr_entry_EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed.sch"/>
   <!-- atcdabbr_entry_LaboratoryReportDataProcessing -->
   <include href="include/1.2.40.0.34.6.0.11.3.25-2023-06-01T135303.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.25-2023-06-01T135303-closed.sch"/>
   <!-- atcdabbr_entry_LaboratoryBatteryOrganizer -->
   <include href="include/1.2.40.0.34.6.0.11.3.26-2023-06-01T135016.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.26-2023-06-01T135016-closed.sch"/>
   <!-- atcdabbr_entry_LaboratoryObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.27-2023-06-01T135820.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.27-2023-06-01T135820-closed.sch"/>
   <!-- elgagab_entry_Konsultationsgrund -->
   <include href="include/1.2.40.0.34.6.0.11.3.30-2021-02-19T104636.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.30-2021-02-19T104636-closed.sch"/>
   <!-- elgagab_entry_KonsultationsgrundProblem -->
   <include href="include/1.2.40.0.34.6.0.11.3.31-2023-02-02T154005.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.31-2023-02-02T154005-closed.sch"/>
   <!-- atcdabbr_entry_CriticalityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed.sch"/>
   <!-- atcdabbr_entry_CertaintyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed.sch"/>
   <!-- atcdabbr_entry_SeverityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed.sch"/>
   <!-- atcdabbr_entry_ProblemStatusObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed.sch"/>
   <!-- atcdabbr_entry_Logo -->
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed.sch"/>
   <!-- atcdabbr_other_PerformerBody -->
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed.sch"/>

</schema>
<!-- versionLabel="1.9.0+20250116-3.0.0" -->
