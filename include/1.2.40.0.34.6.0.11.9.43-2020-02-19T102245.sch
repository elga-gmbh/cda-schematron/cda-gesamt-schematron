<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.43
Name: Device Product Instance Template
Description: Das "Device Product Instance Template" beschreibt die verpflichtenden Eigenschaften des verwendeten Gerätes. Mindestens eine eindeutige ID, die Klassifizierung und der Name (samt Hersteller) des Gerätes sind für jedes verwendetes Gerät anzugeben. Dieses Template definiert das participantRole-Element des Device Information Organizer. In vielen Fällen ist das Gerät
                ein IEEE 11073 kompatibles Gerät. In diesem Dokument vorhandene medizinische Beobachtungen besitzen ein author-Element, welches in den Daten dieses Templates vorhanden sein muss. Alle weiteren Informationen zum verwendeten Gerät können im darüberliegenden Device Information Organizer kodiert werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245">
   <title>Device Product Instance Template</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]"
         id="d41e62942-false-d3618255e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@classCode) = ('MANU')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von classCode MUSS 'MANU' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:scopingEntitiy) = 0">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:scopingEntitiy DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43']"
         id="d41e62952-false-d3618316e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.9.43')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '1.2.40.0.34.6.0.11.9.43' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37']"
         id="d41e62960-false-d3618331e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.22.4.37')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '2.16.840.1.113883.10.20.22.4.37' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']"
         id="d41e62968-false-d3618346e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.9')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.9' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:id[not(@nullFlavor)]"
         id="d41e62979-false-d3618367e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="@root">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @root MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(@root) or matches(@root,'^[0-2](\.(0|[1-9]\d*))*$')">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @root MUSS vom Datentyp 'oid' sein  - '<value-of select="@root"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="@extension">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @extension MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="@assigningAuthorityName">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @assigningAuthorityName MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(@assigningAuthorityName) or string-length(@assigningAuthorityName)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @assigningAuthorityName MUSS vom Datentyp 'st' sein  - '<value-of select="@assigningAuthorityName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]"
         id="d41e62997-false-d3618391e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:code[@codeSystem = '2.16.840.1.113883.6.24']) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:code[@codeSystem = '2.16.840.1.113883.6.24']) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:code[@codeSystem = '2.16.840.1.113883.6.24'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:manufacturerModelName[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:manufacturerModelName[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:manufacturerModelName[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']"
         id="d41e63002-false-d3618417e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="@code">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="string(@codeSystemName) = ('MDC') or not(@codeSystemName)">(atcdabbr_entry_DeviceProductInstanceTemplate): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceProductInstanceTemplate): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:translation
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:code[@codeSystem = '2.16.840.1.113883.6.24']/hl7:translation"
         id="d41e63016-false-d3618445e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:manufacturerModelName[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:playingDevice[hl7:code[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:manufacturerModelName[not(@nullFlavor)]"
         id="d41e63023-false-d3618455e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy"
         id="d41e63030-false-d3618465e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="count(hl7:desc) = 0">(atcdabbr_entry_DeviceProductInstanceTemplate): Element hl7:desc DARF NICHT vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.43
Context: *[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/hl7:desc
Item: (atcdabbr_entry_DeviceProductInstanceTemplate)
-->

   <rule fpi="RULC-1"
         context="*[hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]]/hl7:participantRole[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.43'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.37'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.9'][@extension = '2015-08-17']]/hl7:scopingEntitiy/hl7:desc"
         id="d41e63035-false-d3618479e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.43-2020-02-19T102245.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceProductInstanceTemplate): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
