<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.9.46
Name: Participant Body - Authorized Editor
Description: 
                 Dokumentation der eindeutigen Kennung des GDA (OID aus dem GDA-Index), der einen Eintrag (Entry) bearbeiten darf ("Berechtigter bearbeitender GDA"). Wird durch die zentrale Anwendung automatisch befüllt. 
                 Dient der GDA-Software diejenigen Einträge zu erkennen, die durch den GDA bearbeitet werden dürfen. 
                 Hintergrund: Das Berechtigungssystem erlaubt die Korrektur eines Eintrags nur dem GDA, der den Eintrag ursprünglich erstellt hat (document.author des zugrundeliegenden "Update Immunisierungsstatus"), oder GDA mit der Rolle "Korrekturberechtigte Person". Wurde ein Eintrag durch einen GDA mit der Rolle "Korrekturberechtigte Person" korrigiert, ist fortan die Änderung dieses
                    Eintrags nur mehr GDA mit der Rolle "Korrekturberechtigte Person" möglich. 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910-closed">
   <title>Participant Body - Authorized Editor</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | self::hl7:participantRole[not(@nullFlavor)])]"
         id="d42e65805-true-d2260619e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910.html"
              test="not(.)">(atcdabbr_other_ParticipantBodyAuthorizedEditor)/d42e65805-true-d2260619e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46'] | hl7:participantRole[not(@nullFlavor)] (rule-reference: d42e65805-true-d2260619e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.46']]/hl7:participantRole[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)][not(@nullFlavor)])]"
         id="d42e65847-true-d2260635e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910.html"
              test="not(.)">(atcdabbr_other_ParticipantBodyAuthorizedEditor)/d42e65847-true-d2260635e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)][not(@nullFlavor)] (rule-reference: d42e65847-true-d2260635e0)</assert>
   </rule>
</pattern>
