<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.165
Name: Notification Organizer
Description: Der "Notification Organizer" ermöglicht die CDA Level 3 Codierung von wichtigen Erregern ("Notifiable Condition") bzw. der Dokumentation der separat erfolgten Meldung ans EMS ("Case Identification").
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249">
   <title>Notification Organizer</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]
Item: (atcdabbr_entry_NotificationOrganizer)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]
Item: (atcdabbr_entry_NotificationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]"
         id="d42e21741-false-d1381975e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@classCode) = ('CLUSTER')">(atcdabbr_entry_NotificationOrganizer): Der Wert von classCode MUSS 'CLUSTER' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@moodCode) = ('EVN')">(atcdabbr_entry_NotificationOrganizer): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165']) &gt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165']) &lt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']) &gt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1'] ist required [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']) &lt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atcdabbr_entry_NotificationOrganizer): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.166'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]] | hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]])"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_NotificationOrganizer): Auswahl (hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.166'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]]  oder  hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]) enthält nicht genügend Elemente [min 1x]</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165']
Item: (atcdabbr_entry_NotificationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165']"
         id="d42e21747-false-d1382060e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_NotificationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.165')">(atcdabbr_entry_NotificationOrganizer): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.165' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']
Item: (atcdabbr_entry_NotificationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']"
         id="d42e21755-false-d1382075e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_NotificationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.1.1')">(atcdabbr_entry_NotificationOrganizer): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.1.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:statusCode[@code = 'completed']
Item: (atcdabbr_entry_NotificationOrganizer)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:statusCode[@code = 'completed']"
         id="d42e21763-false-d1382090e0">
      <extends rule="CS"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_entry_NotificationOrganizer): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="@nullFlavor or (@code='completed')">(atcdabbr_entry_NotificationOrganizer): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.166'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]]
Item: (atcdabbr_entry_NotificationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.166'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.1']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@typeCode) = ('COMP')">(atcdabbr_entry_NotificationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.165
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]
Item: (atcdabbr_entry_NotificationOrganizer)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.165'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.170'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1.1.2']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.3.165-2021-02-02T084249.html"
              test="string(@typeCode) = ('COMP')">(atcdabbr_entry_NotificationOrganizer): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
</pattern>
