<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.100
Name: Serienmessung Vitalparameter Entry
Description: Das Serienmessung Entry dokumentiert eine kontinuierliche Messungen eines Gerätes. Eine kontinuierliche Messung beinhaltet mehrere Datenpunkte und eine Zeitspanne. Diese Messwerte sind   als Vitalparameter definiert! 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426-closed">
   <title>Serienmessung Vitalparameter Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']])]"
         id="d42e55496-true-d5169773e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55496-true-d5169773e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']] (rule-reference: d42e55496-true-d5169773e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:value[@nullFlavor='NA'] | self::hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:participant[@typeCode][hl7:participantRole] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]])]"
         id="d42e55593-true-d5170346e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55593-true-d5170346e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09'] | hl7:id[not(@nullFlavor)] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:value[@nullFlavor='NA'] | hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:participant[@typeCode][hl7:participantRole] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]] (rule-reference: d42e55593-true-d5170346e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation | self::ips:designation)]"
         id="d42e55641-true-d5170396e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55641-true-d5170396e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation | ips:designation (rule-reference: d42e55641-true-d5170396e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.34-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e55659-true-d5170410e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55659-true-d5170410e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e55659-true-d5170410e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e55698-true-d5170439e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55698-true-d5170439e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e55698-true-d5170439e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d42e55715-true-d5170462e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55715-true-d5170462e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d42e55715-true-d5170462e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | self::hl7:time | self::hl7:assignedEntity[not(@nullFlavor)])]"
         id="d42e55739-true-d5170621e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55739-true-d5170621e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17'] | hl7:time | hl7:assignedEntity[not(@nullFlavor)] (rule-reference: d42e55739-true-d5170621e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5170482e14-true-d5170727e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5170482e14-true-d5170727e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5170482e14-true-d5170727e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5170731e101-true-d5170800e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5170731e101-true-d5170800e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5170731e101-true-d5170800e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5170731e174-true-d5170876e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5170731e174-true-d5170876e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5170731e174-true-d5170876e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5170731e186-true-d5170898e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5170731e186-true-d5170898e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5170731e186-true-d5170898e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5170886e12-true-d5170927e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.16-2021-05-26T140421.html"
              test="not(.)">( atcdabbr_other_AssignedEntityBody)/d5170886e12-true-d5170927e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5170886e12-true-d5170927e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5170731e198-true-d5170978e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5170731e198-true-d5170978e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5170731e198-true-d5170978e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:performer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.9.17']]/hl7:assignedEntity[not(@nullFlavor)]/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5170952e58-true-d5171039e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5170952e58-true-d5171039e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5170952e58-true-d5171039e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e55741-true-d5171206e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55741-true-d5171206e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e55741-true-d5171206e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d5171084e49-true-d5171314e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171084e49-true-d5171314e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d5171084e49-true-d5171314e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5171084e68-true-d5171373e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171084e68-true-d5171373e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5171084e68-true-d5171373e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5171084e111-true-d5171430e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171084e111-true-d5171430e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5171084e111-true-d5171430e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5171434e91-true-d5171464e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.9.6-2021-02-19T133649.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d5171434e91-true-d5171464e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5171434e91-true-d5171464e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d5171084e134-true-d5171512e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171084e134-true-d5171512e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d5171084e134-true-d5171512e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5171084e150-true-d5171557e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171084e150-true-d5171557e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5171084e150-true-d5171557e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5171527e66-true-d5171620e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171527e66-true-d5171620e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5171527e66-true-d5171620e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e55754-true-d5171788e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55754-true-d5171788e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e55754-true-d5171788e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d5171665e5-true-d5171920e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171665e5-true-d5171920e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d5171665e5-true-d5171920e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5171793e50-true-d5171986e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171793e50-true-d5171986e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5171793e50-true-d5171986e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5171793e120-true-d5172048e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171793e120-true-d5172048e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5171793e120-true-d5172048e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d5171793e132-true-d5172070e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171793e132-true-d5172070e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d5171793e132-true-d5172070e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5172058e12-true-d5172099e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172058e12-true-d5172099e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5172058e12-true-d5172099e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d5171793e143-true-d5172150e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171793e143-true-d5172150e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d5171793e143-true-d5172150e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5172124e58-true-d5172211e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172124e58-true-d5172211e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5172124e58-true-d5172211e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d5171665e7-true-d5172288e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171665e7-true-d5172288e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d5171665e7-true-d5172288e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d5171665e16-true-d5172341e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171665e16-true-d5172341e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d5171665e16-true-d5172341e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d5171665e20-true-d5172396e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5171665e20-true-d5172396e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d5171665e20-true-d5172396e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d5172389e11-true-d5172423e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172389e11-true-d5172423e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d5172389e11-true-d5172423e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/*[not(@xsi:nil = 'true')][not(self::hl7:time | self::hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:participantRole)]"
         id="d42e55756-true-d5172551e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55756-true-d5172551e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:time | hl7:awarenessCode[concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.10310-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:participantRole (rule-reference: d42e55756-true-d5172551e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:playingDevice | self::hl7:playingEntity | self::hl7:scopingEntity)]"
         id="d5172454e11-true-d5172659e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172454e11-true-d5172659e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code | hl7:addr | hl7:telecom | hl7:playingDevice | hl7:playingEntity | hl7:scopingEntity (rule-reference: d5172454e11-true-d5172659e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d5172454e22-true-d5172721e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172454e22-true-d5172721e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d5172454e22-true-d5172721e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingDevice/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:manufacturerModelName | self::hl7:softwareName)]"
         id="d5172454e78-true-d5172799e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172454e78-true-d5172799e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:manufacturerModelName | hl7:softwareName (rule-reference: d5172454e78-true-d5172799e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:playingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:quantity | self::hl7:name | self::sdtc:birthTime | self::hl7:desc)]"
         id="d5172454e80-true-d5172860e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172454e80-true-d5172860e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:quantity | hl7:name | sdtc:birthTime | hl7:desc (rule-reference: d5172454e80-true-d5172860e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:participant[@typeCode][hl7:participantRole]/hl7:participantRole/hl7:scopingEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id | self::hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | self::hl7:desc)]"
         id="d5172454e83-true-d5172918e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172454e83-true-d5172918e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id | hl7:code[@codeSystem = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/completeCodeSystem/@codeSystem or concat(@code, @codeSystem) = doc('include/voc-2.16.840.1.113883.1.11.16040-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] | hl7:desc (rule-reference: d5172454e83-true-d5172918e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']])]"
         id="d42e55758-true-d5172973e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d42e55758-true-d5172973e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']] (rule-reference: d42e55758-true-d5172973e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | self::hl7:code[@nullFlavor='NA'] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | self::hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]])]"
         id="d5172941e6-true-d5173030e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172941e6-true-d5173030e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17'] | hl7:code[@nullFlavor='NA'] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]] | hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]] (rule-reference: d5172941e6-true-d5173030e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']])]"
         id="d5172941e36-true-d5173068e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172941e36-true-d5173068e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']] (rule-reference: d5172941e36-true-d5173068e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | self::hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value)]"
         id="d5173050e7-true-d5173117e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173050e7-true-d5173117e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:value (rule-reference: d5173050e7-true-d5173117e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:translation | self::ips:designation)]"
         id="d5173050e31-true-d5173161e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173050e31-true-d5173161e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:translation | ips:designation (rule-reference: d5173050e31-true-d5173161e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173050e48-true-d5173175e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173050e48-true-d5173175e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173050e48-true-d5173175e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173050e102-true-d5173210e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173050e102-true-d5173210e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173050e102-true-d5173210e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:origin[not(@nullFlavor)] | self::hl7:scale[not(@nullFlavor)] | self::hl7:digits[not(@nullFlavor)])]"
         id="d5173050e119-true-d5173245e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173050e119-true-d5173245e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:origin[not(@nullFlavor)] | hl7:scale[not(@nullFlavor)] | hl7:digits[not(@nullFlavor)] (rule-reference: d5173050e119-true-d5173245e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']])]"
         id="d5172941e46-true-d5173283e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5172941e46-true-d5173283e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']] (rule-reference: d5172941e46-true-d5173283e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | self::hl7:code[not(@nullFlavor)] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)])]"
         id="d5173265e7-true-d5173323e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173265e7-true-d5173323e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17'] | hl7:code[not(@nullFlavor)] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)] (rule-reference: d5173265e7-true-d5173323e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d5173265e41-true-d5173354e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173265e41-true-d5173354e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d5173265e41-true-d5173354e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.100'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.51'][@extension = '2015-11-25'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.27'][@extension = '2014-06-09']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.102'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.37'][@extension = '2015-08-17']]/hl7:entryRelationship[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.104'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.13'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:head[not(@nullFlavor)] | self::hl7:increment[not(@nullFlavor)])]"
         id="d5173265e53-true-d5173378e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.100-2020-06-02T102426.html"
              test="not(.)">(atcdabbr_entry_SerienmessungVitalparameterEntry)/d5173265e53-true-d5173378e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:head[not(@nullFlavor)] | hl7:increment[not(@nullFlavor)] (rule-reference: d5173265e53-true-d5173378e0)</assert>
   </rule>
</pattern>
