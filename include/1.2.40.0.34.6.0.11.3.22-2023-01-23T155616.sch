<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.22
Name: Impfplan Entry
Description: 
                 Referenz auf die Information, die der Impfempfehlung zugrunde liegt, z.B.: 
                 
                     
                         Nationaler Impfplan ("Impfplan Österreich") 
                     
                     
                         e-Impfplan Regelwerk - beinhaltet Informationen aus dem Impfplan Österreich und teilweise aus den Fachinformationen, sollte diese im Impfplan Österreich zu wenig spezifiziert sein. 
                     
                     
                         weitere Fachinformation (liegt derzeit nicht strukturiert vor) 
                     
                     
                         Individuelle Konstellation des Impflings -> manueller Eintrag bei Impfung 
                     
                 
                 (Abhängig vom Produkt bzw. "Impfung", wenn Kombinationspräparat) 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616">
   <title>Impfplan Entry</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]
Item: (atcdabbr_entry_impfplan)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]"
         id="d42e39482-false-d1366907e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="string(@classCode) = ('DOC') or not(@classCode)">(atcdabbr_entry_impfplan): Der Wert von classCode MUSS 'DOC' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atcdabbr_entry_impfplan): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']) &gt;= 1">(atcdabbr_entry_impfplan): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_impfplan): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:code[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:code[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_impfplan): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:setId[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:setId[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="count(hl7:versionNumber[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_impfplan): Element hl7:versionNumber[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']"
         id="d42e39488-false-d1366967e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_impfplan): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.22')">(atcdabbr_entry_impfplan): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.22' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:id[not(@nullFlavor)]"
         id="d42e39498-false-d1366981e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_impfplan): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:code[not(@nullFlavor)]
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:code[not(@nullFlavor)]"
         id="d42e39516-false-d1366989e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_impfplan): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:text
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:text"
         id="d1366990e65-false-d1367000e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabrr_other_NarrativeTextReference): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.1
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:text/hl7:reference[not(@nullFlavor)]
Item: (atcdabrr_other_NarrativeTextReference)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:text/hl7:reference[not(@nullFlavor)]"
         id="d1366990e67-false-d1367019e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabrr_other_NarrativeTextReference): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="@value">(atcdabrr_other_NarrativeTextReference): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="starts-with(@value,'#') or starts-with(@value,'http')">(atcdabrr_other_NarrativeTextReference): The @value attribute content MUST conform to the format '#xxx', where xxx is the ID of the corresponding 'content'-element, or begin with the 'http' or 'https' url-scheme.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:setId[not(@nullFlavor)]
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:setId[not(@nullFlavor)]"
         id="d42e39534-false-d1367031e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_impfplan): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.22
Context: *[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:versionNumber[not(@nullFlavor)]
Item: (atcdabbr_entry_impfplan)
-->

   <rule fpi="RULC-1"
         context="*[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.22']]/hl7:versionNumber[not(@nullFlavor)]"
         id="d42e39540-false-d1367039e0">
      <extends rule="INT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')">(atcdabbr_entry_impfplan): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:INT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.html"
              test="not(@value) or matches(@value, '^-?[1-9]\d*$|^+?\d*$')">(atcdabbr_entry_impfplan): @value ist keine gültige INT Zahl <value-of select="@value"/>
      </assert>
   </rule>
</pattern>
