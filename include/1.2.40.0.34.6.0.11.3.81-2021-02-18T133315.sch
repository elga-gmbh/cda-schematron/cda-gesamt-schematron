<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.81
Name: Device Specification Unspecified Information Observation
Description: Die Device Specification Unspecified Information Observation dokumentiert Informationen über das Gerät, welches nicht in den vorhandenen vordefinierten Elementen dokumentiert werden kann. In PCHA konformen Geräten, kommen die Informationen aus dem Production Specification attribute. Diese Information wird nicht übertragen von Bluetooth Low Energy health
                    devices.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315">
   <title>Device Specification Unspecified Information Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]"
         id="d41e53843-false-d3605585e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81']) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81']) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81']
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81']"
         id="d41e53849-false-d3605654e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.81')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.81' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17']"
         id="d41e53858-false-d3605669e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.17')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.17' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']"
         id="d41e53869-false-d3605691e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.25')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.25' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e53881-false-d3605713e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@code) = ('531971')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von code MUSS '531971' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="string(@displayName) = ('MDC_ID_PROD_SPEC_UNSPECIFIED: Unspecified but additional information')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Der Wert von displayName MUSS 'MDC_ID_PROD_SPEC_UNSPECIFIED: Unspecified but additional information' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e53891-false-d3605751e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="@code">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="@codeSystem">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:code[(@code = '531971' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e53906-false-d3605778e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="@language">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]"
         id="d41e53915-false-d3605794e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]"
         id="d41e53920-false-d3605813e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="@value">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.81
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]
Item: (atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.81'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.25'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]"
         id="d41e53926-false-d3605829e0">
      <extends rule="ST"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.html"
              test="@nullFlavor or ($xsiLocalName='ST' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
