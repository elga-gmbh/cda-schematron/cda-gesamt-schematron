<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.777.3.10.15
Name: aktive Medizinprodukte
Description: Diese Sektion enthält Informationen über intra- und extrakorporale Medizinprodukte oder Medizingeräte, von denen der Gesundheitszustand des Patienten direkt abhängig ist. Das umfasst z.B. Implantate, Prothesen, Pumpen, Herzschrittmacher etc. von denen ein GDA Kenntnis haben soll. Heilbehelfe wie Gehhilfen, Rollstuhl etc sind nicht notwendigerweise
                    anzuführen.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.777.3.10.15-2019-02-28T100447">
   <title>aktive Medizinprodukte</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]"
         id="d41e65146-false-d3623277e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']) &gt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:templateId[@root = '1.2.40.0.34.777.3.10.15'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']) &lt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:templateId[@root = '1.2.40.0.34.777.3.10.15'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:id) &lt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:id kommt zu häufig vor [max 1x].</assert>
      <report fpi="CD-UNKN-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt; 1">(tgd_section_aktiveMedizinprodukte): Element hl7:code ist codiert mit Bindungsstärke 'extensible' und enthält ein Code außerhalb des angegebene Wertraums.</report>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="count(hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]) &gt;= 1">(tgd_section_aktiveMedizinprodukte): Element hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]] ist required [min 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']"
         id="d41e65148-false-d3623363e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(tgd_section_aktiveMedizinprodukte): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="string(@root) = ('1.2.40.0.34.777.3.10.15')">(tgd_section_aktiveMedizinprodukte): Der Wert von root MUSS '1.2.40.0.34.777.3.10.15' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:id
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:id"
         id="d41e65153-false-d3623377e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(tgd_section_aktiveMedizinprodukte): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:code[(@code = '46264-8' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d41e65160-false-d3623388e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(tgd_section_aktiveMedizinprodukte): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="@nullFlavor or (@code='46264-8' and @codeSystem='2.16.840.1.113883.6.1')">(tgd_section_aktiveMedizinprodukte): Der Elementinhalt MUSS einer von 'code '46264-8' codeSystem '2.16.840.1.113883.6.1'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:title[not(@nullFlavor)]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:title[not(@nullFlavor)]"
         id="d41e65165-false-d3623404e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(tgd_section_aktiveMedizinprodukte): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="text()='Aktive Medizinprodukte'">(tgd_section_aktiveMedizinprodukte): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Aktive Medizinprodukte'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:text[not(@nullFlavor)]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:text[not(@nullFlavor)]"
         id="d41e65171-false-d3623418e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(tgd_section_aktiveMedizinprodukte): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]
Item: (tgd_section_aktiveMedizinprodukte)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@typeCode) = ('AUT') or not(@typeCode)">(AuthorBody_PS): Der Wert von typeCode MUSS 'AUT' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@contextControlCode) = ('OP') or not(@contextControlCode)">(AuthorBody_PS): Der Wert von contextControlCode MUSS 'OP' sein. Gefunden: "<value-of select="@contextControlCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:time) &gt;= 1">(AuthorBody_PS): Element hl7:time ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:time) &lt;= 1">(AuthorBody_PS): Element hl7:time kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthor[hl7:representedOrganization]) &gt;= 1">(AuthorBody_PS): Element hl7:assignedAuthor[hl7:representedOrganization] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthor[hl7:representedOrganization]) &lt;= 1">(AuthorBody_PS): Element hl7:assignedAuthor[hl7:representedOrganization] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:time
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:time">
      <extends rule="TS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="not(*)">(AuthorBody_PS): <value-of select="local-name()"/> with datatype TS, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('ASSIGNED') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'ASSIGNED' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:id) &gt;= 1">(AuthorBody_PS): Element hl7:id ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(AuthorBody_PS): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:assignedPerson | hl7:assignedAuthoringDevice)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="$elmcount &gt;= 1">(AuthorBody_PS): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="$elmcount &lt;= 1">(AuthorBody_PS): Auswahl (hl7:assignedPerson  oder  hl7:assignedAuthoringDevice) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedPerson) &lt;= 1">(AuthorBody_PS): Element hl7:assignedPerson kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:assignedAuthoringDevice) &lt;= 1">(AuthorBody_PS): Element hl7:assignedAuthoringDevice kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:representedOrganization[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:representedOrganization[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:representedOrganization[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:id
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:id">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(AuthorBody_PS): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.6 ELGA_AuthorSpeciality (DYNAMIC)' sein.</assert>
      <report fpi="CD-DEPR-BSP"
              role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="exists(doc('include/voc-1.2.40.0.34.10.6-DYNAMIC.xml')//valueSet[1]/conceptList/concept[@type='D'][@code = $theCode][@codeSystem = $theCodeSystem])">(AuthorBody_PS): Element hl7:code ist codiert mit Bindungsstärke 'required' und enthält einen Code der veraltet ist.</report>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:addr
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:telecom
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:telecom">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('PSN') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'PSN' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.90001
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]
Item: (PersonElements)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedPerson/hl7:name[not(@nullFlavor)]">
      <extends rule="PN"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.90001-2011-12-19T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PN')">(PersonElements): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PN" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('DEV') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'DEV' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:manufacturerModelName) &gt;= 1">(AuthorBody_PS): Element hl7:manufacturerModelName ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:manufacturerModelName) &lt;= 1">(AuthorBody_PS): Element hl7:manufacturerModelName kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:softwareName) &gt;= 1">(AuthorBody_PS): Element hl7:softwareName ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:softwareName) &lt;= 1">(AuthorBody_PS): Element hl7:softwareName kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:manufacturerModelName">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:assignedAuthoringDevice/hl7:softwareName">
      <extends rule="SC"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SC')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SC" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@classCode) = ('ORG') or not(@classCode)">(AuthorBody_PS): Der Wert von classCode MUSS 'ORG' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="string(@determinerCode) = ('INSTANCE') or not(@determinerCode)">(AuthorBody_PS): Der Wert von determinerCode MUSS 'INSTANCE' sein. Gefunden: "<value-of select="@determinerCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &gt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:name[not(@nullFlavor)]) &lt;= 1">(AuthorBody_PS): Element hl7:name[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="count(hl7:addr) &lt;= 1">(AuthorBody_PS): Element hl7:addr kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:id[not(@nullFlavor)]">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:name[not(@nullFlavor)]">
      <extends rule="ON"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ON')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ON" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:telecom">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.13.3.16
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr
Item: (AuthorBody_PS)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor[hl7:representedOrganization]/hl7:representedOrganization[not(@nullFlavor)]/hl7:addr">
      <extends rule="AD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.11.13.3.16-2017-08-05T190445.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'AD')">(AuthorBody_PS): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:AD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.777.3.10.15
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]
Item: (tgd_section_aktiveMedizinprodukte)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.777.3.10.15']]/hl7:entry[hl7:supply[hl7:templateId[@root = '1.2.40.0.34.11.13.3.8']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.777.3.10.15-2019-02-28T100447.html"
              test="string(@typeCode) = ('DRIV')">(tgd_section_aktiveMedizinprodukte): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
</pattern>
