<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.93
Name: EMS Organizer Arzt
Description: Der Laboratory Battery Organizer ist ein Strukturierungshilfsmittel und beinhaltet eine Sammlung von Laboratory Observations. Laut ELGA Implementierungsleitfaden für den Laborbefund kann ein Befund mehrere Organizer enthalten und jeweils Ergebnisse einer Befundgruppe beinhalten. Die EMS Arztmeldung enthält jedoch nur einen Organizer welcher die benötigten EMS
                Parameter als einzelne Observations geführt werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630-closed">
   <title>EMS Organizer Arzt</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/*[not(@xsi:nil = 'true')][not(self::hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']])]"
         id="d42e23082-true-d291645e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(.)">(epims_entry_EMSOrganizerArzt)/d42e23082-true-d291645e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']] (rule-reference: d42e23082-true-d291645e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] | self::hl7:templateId[@root = '1.2.40.0.34.11.6.2.1'] | self::hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')] | self::hl7:statusCode[@code = 'completed'] | self::hl7:effectiveTime | self::hl7:component[@typeCode = 'COMP'] | self::hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]])]"
         id="d42e23115-true-d291727e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(.)">(epims_entry_EMSOrganizerArzt)/d42e23115-true-d291727e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] | hl7:templateId[@root = '1.2.40.0.34.11.6.2.1'] | hl7:code[(@code = '30' and @codeSystem = '1.2.40.0.34.5.189')] | hl7:statusCode[@code = 'completed'] | hl7:effectiveTime | hl7:component[@typeCode = 'COMP'] | hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]] (rule-reference: d42e23115-true-d291727e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:effectiveTime/*[not(@xsi:nil = 'true')][not(self::hl7:low[@value] | self::hl7:low[@nullFlavor='UNK'] | self::hl7:high[@value] | self::hl7:high[@nullFlavor='UNK'])]"
         id="d42e23142-true-d291766e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(.)">(epims_entry_EMSOrganizerArzt)/d42e23142-true-d291766e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[@value] | hl7:low[@nullFlavor='UNK'] | hl7:high[@value] | hl7:high[@nullFlavor='UNK'] (rule-reference: d42e23142-true-d291766e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/*[not(@xsi:nil = 'true')][not(self::hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN'])]"
         id="d42e23150-true-d291803e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(.)">(epims_entry_EMSOrganizerArzt)/d42e23150-true-d291803e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN'] (rule-reference: d42e23150-true-d291803e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:effectiveTime | self::hl7:value)]"
         id="d291807e17-true-d291829e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="not(.)">(epims_entry_EmsOrganizerObservation)/d291807e17-true-d291829e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:effectiveTime | hl7:value (rule-reference: d291807e17-true-d291829e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:qualifier)]"
         id="d291807e31-true-d291853e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="not(.)">(epims_entry_EmsOrganizerObservation)/d291807e31-true-d291853e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:qualifier (rule-reference: d291807e31-true-d291853e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[@typeCode = 'COMP']/hl7:observation[not(@nullFlavor)][@classCode = 'OBS'][@moodCode = 'EVN']/hl7:value/hl7:qualifier/*[not(@xsi:nil = 'true')][not(self::hl7:name | self::hl7:value)]"
         id="d291807e37-true-d291872e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.69-2020-03-24T104519.html"
              test="not(.)">(epims_entry_EmsOrganizerObservation)/d291807e37-true-d291872e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name | hl7:value (rule-reference: d291807e37-true-d291872e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]])]"
         id="d42e23163-true-d291900e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.93-2020-04-22T154630.html"
              test="not(.)">(epims_entry_EMSOrganizerArzt)/d42e23163-true-d291900e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]] (rule-reference: d42e23163-true-d291900e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/*[not(@xsi:nil = 'true')][not(self::hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')] | self::hl7:effectiveTime | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')])]"
         id="d291904e20-true-d291929e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="not(.)">(epims_entry_foreignCountry)/d291904e20-true-d291929e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')] | hl7:effectiveTime | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')] (rule-reference: d291904e20-true-d291929e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/*[not(@xsi:nil = 'true')][not(self::hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]])]"
         id="d291904e37-true-d291956e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="not(.)">(epims_entry_foreignCountry)/d291904e37-true-d291956e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]] (rule-reference: d291904e37-true-d291956e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.93'] and hl7:templateId[@root = '1.2.40.0.34.11.6.2.1']]/hl7:component[hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]]/hl7:observation[hl7:code[(@code = 'ILLOC' and @codeSystem = '1.2.40.0.34.5.101')]]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')]/hl7:qualifier[hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')]]/*[not(@xsi:nil = 'true')][not(self::hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')] | self::hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)])]"
         id="d291904e45-true-d291984e0">
      <assert role="error"
              see="https://elga.art-decor.org/epims-html-20250116T123132/tmp-1.2.40.0.34.6.0.11.3.62-2020-02-20T162957.html"
              test="not(.)">(epims_entry_foreignCountry)/d291904e45-true-d291984e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[(@code = 'TRVCNTRY' and @codeSystem = '1.2.40.0.34.5.101')] | hl7:value[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.6.0.10.51-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] (rule-reference: d291904e45-true-d291984e0)</assert>
   </rule>
</pattern>
