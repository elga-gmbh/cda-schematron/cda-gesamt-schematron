<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.5.2.5
Name: Aktuelle Untersuchung
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.5.2.5-2012-01-12T000000">
   <title>Aktuelle Untersuchung</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]
Item: (AktuelleUntersuchung)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]
Item: (AktuelleUntersuchung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]"
         id="d42e32588-false-d333825e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(AktuelleUntersuchung): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']) &gt;= 1">(AktuelleUntersuchung): Element hl7:templateId[@root = '1.2.40.0.34.11.5.2.5'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']) &lt;= 1">(AktuelleUntersuchung): Element hl7:templateId[@root = '1.2.40.0.34.11.5.2.5'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(AktuelleUntersuchung): Element hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(AktuelleUntersuchung): Element hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(AktuelleUntersuchung): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(AktuelleUntersuchung): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(AktuelleUntersuchung): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(AktuelleUntersuchung): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']
Item: (AktuelleUntersuchung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']"
         id="d42e32594-false-d333879e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(AktuelleUntersuchung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.5.2.5')">(AktuelleUntersuchung): Der Wert von root MUSS '1.2.40.0.34.11.5.2.5' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (AktuelleUntersuchung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:code[(@code = '55111-9' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e32601-false-d333894e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(AktuelleUntersuchung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="@nullFlavor or (@code='55111-9' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Current imaging procedure descriptions' and @codeSystemName='LOINC')">(AktuelleUntersuchung): Der Elementinhalt MUSS einer von 'code '55111-9' codeSystem '2.16.840.1.113883.6.1' displayName='Current imaging procedure descriptions' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:title[not(@nullFlavor)]
Item: (AktuelleUntersuchung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:title[not(@nullFlavor)]"
         id="d42e32609-false-d333910e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(AktuelleUntersuchung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="text()='Aktuelle Untersuchung'">(AktuelleUntersuchung): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Aktuelle Untersuchung'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:text[not(@nullFlavor)]
Item: (AktuelleUntersuchung)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:text[not(@nullFlavor)]"
         id="d42e32617-false-d333924e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.5.2.5-2012-01-12T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(AktuelleUntersuchung): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.5
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.5']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]
Item: (AktuelleUntersuchung)
--></pattern>
