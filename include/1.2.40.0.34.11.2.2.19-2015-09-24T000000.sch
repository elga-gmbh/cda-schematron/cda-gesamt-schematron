<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.19
Name: Frühere Erkrankungen
Description: Liste der bisherigen Krankheiten des Patienten.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.19-2015-09-24T000000">
   <title>Frühere Erkrankungen</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]
Item: (FruehereErkrankungen)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]
Item: (FruehereErkrankungen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]"
         id="d42e9058-false-d173689e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']) &gt;= 1">(FruehereErkrankungen): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.19'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']) &lt;= 1">(FruehereErkrankungen): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.19'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(FruehereErkrankungen): Element hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(FruehereErkrankungen): Element hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(FruehereErkrankungen): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(FruehereErkrankungen): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(FruehereErkrankungen): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(FruehereErkrankungen): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']
Item: (FruehereErkrankungen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']"
         id="d42e9062-false-d173732e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(FruehereErkrankungen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.19')">(FruehereErkrankungen): Der Wert von root MUSS '1.2.40.0.34.11.2.2.19' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (FruehereErkrankungen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:code[(@code = '11348-0' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e9069-false-d173747e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(FruehereErkrankungen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="@nullFlavor or (@code='11348-0' and @codeSystem='2.16.840.1.113883.6.1')">(FruehereErkrankungen): Der Elementinhalt MUSS einer von 'code '11348-0' codeSystem '2.16.840.1.113883.6.1'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:title[not(@nullFlavor)]
Item: (FruehereErkrankungen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:title[not(@nullFlavor)]"
         id="d42e9080-false-d173763e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(FruehereErkrankungen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="text()='Frühere Erkrankungen'">(FruehereErkrankungen): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Frühere Erkrankungen'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.19
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:text[not(@nullFlavor)]
Item: (FruehereErkrankungen)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.19']]/hl7:text[not(@nullFlavor)]"
         id="d42e9088-false-d173777e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.19-2015-09-24T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(FruehereErkrankungen): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
