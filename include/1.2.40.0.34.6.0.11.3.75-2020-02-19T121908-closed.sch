<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.75
Name: Device GMDN Information Observation
Description: Die Device GMDN Information Observation dokumentiert die GMDN (Global Medical Device Nomenclature) des verwendeten Gerätes. In PCHA konformen Geräten, sind das Informationen aus dem Production Specification Attribut. Diese Information wird nicht übertragen von Bluetooth Low Energy health devices.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.75-2020-02-19T121908-closed">
   <title>Device GMDN Information Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']])]"
         id="d41e52043-true-d3600491e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.75-2020-02-19T121908.html"
              test="not(.)">(atcdabbr_entry_DeviceGMDNInformationObservation)/d41e52043-true-d3600491e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']] (rule-reference: d41e52043-true-d3600491e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17'] | self::hl7:code[(@code = '531978' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')])]"
         id="d41e52087-true-d3600549e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.75-2020-02-19T121908.html"
              test="not(.)">(atcdabbr_entry_DeviceGMDNInformationObservation)/d41e52087-true-d3600549e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17'] | hl7:code[(@code = '531978' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] (rule-reference: d41e52087-true-d3600549e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]/hl7:code[(@code = '531978' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e52136-true-d3600593e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.75-2020-02-19T121908.html"
              test="not(.)">(atcdabbr_entry_DeviceGMDNInformationObservation)/d41e52136-true-d3600593e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e52136-true-d3600593e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.75'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.17'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.19'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e52170-true-d3600617e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.75-2020-02-19T121908.html"
              test="not(.)">(atcdabbr_entry_DeviceGMDNInformationObservation)/d41e52170-true-d3600617e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e52170-true-d3600617e0)</assert>
   </rule>
</pattern>
