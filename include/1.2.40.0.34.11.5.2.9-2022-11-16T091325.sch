<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.5.2.9
Name: Befund
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.5.2.9-2022-11-16T091325">
   <title>Befund</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]
Item: (Befund)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]
Item: (Befund)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]"
         id="d42e14563-false-d125874e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(Befund): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']) &gt;= 1">(Befund): Element hl7:templateId[@root = '1.2.40.0.34.11.5.2.9'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']) &lt;= 1">(Befund): Element hl7:templateId[@root = '1.2.40.0.34.11.5.2.9'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(Befund): Element hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(Befund): Element hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(Befund): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(Befund): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(Befund): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(Befund): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']
Item: (Befund)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']"
         id="d42e14567-false-d125943e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(Befund): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="string(@root) = ('1.2.40.0.34.11.5.2.9')">(Befund): Der Wert von root MUSS '1.2.40.0.34.11.5.2.9' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (Befund)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:code[(@code = '18782-3' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e14572-false-d125958e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(Befund): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="@nullFlavor or (@code='18782-3' and @codeSystem='2.16.840.1.113883.6.1' and @displayName='Study observation' and @codeSystemName='LOINC')">(Befund): Der Elementinhalt MUSS einer von 'code '18782-3' codeSystem '2.16.840.1.113883.6.1' displayName='Study observation' codeSystemName='LOINC'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:title[not(@nullFlavor)]
Item: (Befund)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:title[not(@nullFlavor)]"
         id="d42e14577-false-d125974e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(Befund): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="text()='Befund'">(Befund): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Befund'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:text[not(@nullFlavor)]
Item: (Befund)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:text[not(@nullFlavor)]"
         id="d42e14583-false-d125988e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.5.2.9-2022-11-16T091325.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(Befund): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.5.3.2'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.6.2.12']]]
Item: (Befund)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:entry[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.5.3.1']]]
Item: (Befund)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.5.2.9
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.5.2.9']]/hl7:entry[hl7:observationMedia[hl7:templateId[@root = '1.2.40.0.34.11.1.3.1']]]
Item: (Befund)
--></pattern>
