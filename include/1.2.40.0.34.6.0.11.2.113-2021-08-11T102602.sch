<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.113
Name: Laboratory Specialty Section (Mikroskopie) - uncodiert
Description: Die Section "Laboratory Specialty Section (Mikroskopie)" entspricht jenem Befundbereich eines Mikrobiologiebefundes, in dem alle Mikroskopie-Ergebnisse dokumentiert werden.  Darstellung der Ergebnisse  "section/text" enthält den narrativen Text, der der CDA Level 2 Darstellung der medizinischen Inhalte entspricht.   Die
                Darstellung der Mikroskopieergebnisse in "section/text" hat tabellarisch zu erfolgen, wobei die Tabelle wie folgt aufgebaut sein sollte. Die Optionalität in dieser Tabelle bezieht sich auf die Darstellung des jeweiligen Elements in der Tabelle. Die Optionalität ist wie folgt zu verstehen: z.B. ist das "Ergebnis" in dieser Tabelle mit [R] angegeben, d.h. das Tabellenelement ist
                verpflichtend anzugeben, befüllt muss es aber nur werden, wenn tatsächlich ein Ergebnis vorhanden ist.  Spaltenname Optionalität Beschreibung Eigenschaft [R] Bezeichung der Analyse (MUSS dem "Begriff"/"displayName" aus dem Value Set "ELGA_Laborparameter"
           
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602">
   <title>Laboratory Specialty Section (Mikroskopie) - uncodiert</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]"
         id="d42e17146-false-d1150150e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']) = 0">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1'] DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']"
         id="d42e17152-false-d1150269e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.113')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.113' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']"
         id="d42e17160-false-d1150281e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.3.2.1')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.3.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:id[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:id[not(@nullFlavor)]"
         id="d42e17173-false-d1150293e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d42e17180-false-d1150304e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="@nullFlavor or (@code='395538009' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='Microscopic specimen observation (finding)')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Elementinhalt MUSS einer von 'code '395538009' codeSystem '2.16.840.1.113883.6.96' displayName='Microscopic specimen observation (finding)'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@codeSystemName) = ('SNOMED CT') or not(@codeSystemName)">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:title[not(@nullFlavor)]"
         id="d42e17190-false-d1150326e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="text()='Mikroskopie'">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Mikroskopie'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:text[not(@nullFlavor)]"
         id="d42e17196-false-d1150340e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.113
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.113']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.113-2021-08-11T102602.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySectionMikroskopieUncodiert): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
