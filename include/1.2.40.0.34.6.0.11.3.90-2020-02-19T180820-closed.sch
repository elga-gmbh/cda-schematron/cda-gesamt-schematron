<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.90
Name: Device Measurement Range Observation
Description: Die Device Measurement Range Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Der Messbereich oder die Messspanne einer Messung sagt aus, in welchem Bereich ein Messwert liegen kann, um noch vom Gerät gemessen werden zu können. Beispielsweise kann der Messbereich eines Thermometers zwischen 0 und 100 Grad
                Celsius liegen. Der Messbereich wird hier in den Einheiten der Messgröße angegeben und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Measurement Range Observation-Elemente existieren. Wenn das Gerät die Daten über den Messbereich oder die anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege
                eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820-closed">
   <title>Device Measurement Range Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']])]"
         id="d41e56428-true-d3613997e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(.)">(atcdabbr_entry_DeviceMeasurementRangeObservation)/d41e56428-true-d3613997e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']] (rule-reference: d41e56428-true-d3613997e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17'] | self::hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)][hl7:reference] | self::hl7:value[@xsi:type='IVL_PQ'] | self::hl7:value[@xsi:type='ST'])]"
         id="d41e56492-true-d3614034e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(.)">(atcdabbr_entry_DeviceMeasurementRangeObservation)/d41e56492-true-d3614034e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17'] | hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)][hl7:reference] | hl7:value[@xsi:type='IVL_PQ'] | hl7:value[@xsi:type='ST'] (rule-reference: d41e56492-true-d3614034e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:code[(@code = '67198' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e56518-true-d3614066e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(.)">(atcdabbr_entry_DeviceMeasurementRangeObservation)/d41e56518-true-d3614066e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e56518-true-d3614066e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)][hl7:reference]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e56553-true-d3614090e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(.)">(atcdabbr_entry_DeviceMeasurementRangeObservation)/d41e56553-true-d3614090e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e56553-true-d3614090e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.90'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.5'][@extension = '2015-08-17']]/hl7:value[@xsi:type='IVL_PQ']/*[not(@xsi:nil = 'true')][not(self::hl7:low[not(@nullFlavor)] | self::hl7:high[not(@nullFlavor)])]"
         id="d41e56566-true-d3614112e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.90-2020-02-19T180820.html"
              test="not(.)">(atcdabbr_entry_DeviceMeasurementRangeObservation)/d41e56566-true-d3614112e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:low[not(@nullFlavor)] | hl7:high[not(@nullFlavor)] (rule-reference: d41e56566-true-d3614112e0)</assert>
   </rule>
</pattern>
