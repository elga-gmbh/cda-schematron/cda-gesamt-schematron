<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.46
Name: Bisherige Schwangerschaften Entry
Description: 
                 Informationen über bisherige Schwangerschaften. 
                 
                     
                 
            
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027-closed">
   <title>Bisherige Schwangerschaften Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']])]"
         id="d42e48588-true-d2054682e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48588-true-d2054682e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']] (rule-reference: d42e48588-true-d2054682e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28'] | self::hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | self::hl7:text | self::hl7:statusCode[@code = 'completed'] | self::hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | self::hl7:author[hl7:assignedAuthor] | self::hl7:informant | self::hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]])]"
         id="d42e48644-true-d2055038e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48644-true-d2055038e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] | hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28'] | hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)] | hl7:text | hl7:statusCode[@code = 'completed'] | hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'INT')] | hl7:author[hl7:assignedAuthor] | hl7:informant | hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]] (rule-reference: d42e48644-true-d2055038e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText | self::hl7:translation | self::ips:designation)]"
         id="d42e48669-true-d2055079e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48669-true-d2055079e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText | hl7:translation | ips:designation (rule-reference: d42e48669-true-d2055079e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.203-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem)]/hl7:originalText/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2055083e41-true-d2055095e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.2-2021-02-19T133148.html"
              test="not(.)">(atcdabbr_other_OriginalTextReference)/d2055083e41-true-d2055095e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2055083e41-true-d2055095e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2055114e54-true-d2055126e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2055114e54-true-d2055126e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2055114e54-true-d2055126e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/*[not(@xsi:nil = 'true')][not(self::hl7:functionCode | self::hl7:time[not(@nullFlavor)] | self::hl7:time[@nullFlavor='UNK'] | self::hl7:assignedAuthor)]"
         id="d42e48729-true-d2055269e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48729-true-d2055269e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:functionCode | hl7:time[not(@nullFlavor)] | hl7:time[@nullFlavor='UNK'] | hl7:assignedAuthor (rule-reference: d42e48729-true-d2055269e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:assignedPerson | self::hl7:assignedAuthoringDevice | self::hl7:representedOrganization)]"
         id="d2055147e46-true-d2055377e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055147e46-true-d2055377e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='UNK'] | hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:assignedPerson | hl7:assignedAuthoringDevice | hl7:representedOrganization (rule-reference: d2055147e46-true-d2055377e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2055147e65-true-d2055436e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055147e65-true-d2055436e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2055147e65-true-d2055436e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2055147e111-true-d2055493e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055147e111-true-d2055493e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2055147e111-true-d2055493e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2055497e92-true-d2055527e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.6-2023-03-31T111555.html"
              test="not(.)">(atcdabbr_other_PersonNameCompilationG2)/d2055497e92-true-d2055527e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2055497e92-true-d2055527e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:assignedAuthoringDevice/*[not(@xsi:nil = 'true')][not(self::hl7:manufacturerModelName[not(@nullFlavor)] | self::hl7:softwareName[not(@nullFlavor)])]"
         id="d2055147e134-true-d2055575e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055147e134-true-d2055575e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:manufacturerModelName[not(@nullFlavor)] | hl7:softwareName[not(@nullFlavor)] (rule-reference: d2055147e134-true-d2055575e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2055147e150-true-d2055620e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055147e150-true-d2055620e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2055147e150-true-d2055620e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:author[hl7:assignedAuthor]/hl7:assignedAuthor/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode[not(@nullFlavor)] | self::hl7:city[not(@nullFlavor)] | self::hl7:state | self::hl7:country[not(@nullFlavor)] | self::hl7:additionalLocator)]"
         id="d2055590e67-true-d2055683e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055590e67-true-d2055683e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode[not(@nullFlavor)] | hl7:city[not(@nullFlavor)] | hl7:state | hl7:country[not(@nullFlavor)] | hl7:additionalLocator (rule-reference: d2055590e67-true-d2055683e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/*[not(@xsi:nil = 'true')][not(self::hl7:assignedEntity | self::hl7:relatedEntity[@classCode = 'PRS'])]"
         id="d42e48736-true-d2055851e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48736-true-d2055851e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:assignedEntity | hl7:relatedEntity[@classCode = 'PRS'] (rule-reference: d42e48736-true-d2055851e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:id[@nullFlavor='NI'] | self::hl7:id[@nullFlavor='UNK'] | self::hl7:code | self::hl7:addr | self::hl7:telecom | self::hl7:assignedPerson | self::hl7:assignedPerson | self::hl7:representedOrganization)]"
         id="d2055728e15-true-d2055983e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055728e15-true-d2055983e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:id[@nullFlavor='NI'] | hl7:id[@nullFlavor='UNK'] | hl7:code | hl7:addr | hl7:telecom | hl7:assignedPerson | hl7:assignedPerson | hl7:representedOrganization (rule-reference: d2055728e15-true-d2055983e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:addr/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2055856e50-true-d2056049e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055856e50-true-d2056049e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2055856e50-true-d2056049e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2055856e120-true-d2056111e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055856e120-true-d2056111e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2055856e120-true-d2056111e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)])]"
         id="d2055856e132-true-d2056133e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055856e132-true-d2056133e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] (rule-reference: d2055856e132-true-d2056133e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:assignedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2056121e12-true-d2056162e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2056121e12-true-d2056162e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2056121e12-true-d2056162e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/*[not(@xsi:nil = 'true')][not(self::hl7:id[not(@nullFlavor)] | self::hl7:name[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)])]"
         id="d2055856e143-true-d2056213e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055856e143-true-d2056213e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:id[not(@nullFlavor)] | hl7:name[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] (rule-reference: d2055856e143-true-d2056213e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:assignedEntity/hl7:representedOrganization/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2056187e58-true-d2056274e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2056187e58-true-d2056274e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2056187e58-true-d2056274e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/*[not(@xsi:nil = 'true')][not(self::hl7:code[not(@nullFlavor)] | self::hl7:addr[not(@nullFlavor)] | self::hl7:telecom[not(@nullFlavor)] | self::hl7:relatedPerson)]"
         id="d2055728e17-true-d2056351e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055728e17-true-d2056351e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:code[not(@nullFlavor)] | hl7:addr[not(@nullFlavor)] | hl7:telecom[not(@nullFlavor)] | hl7:relatedPerson (rule-reference: d2055728e17-true-d2056351e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:addr[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:streetAddressLine | self::hl7:streetName | self::hl7:houseNumber | self::hl7:postalCode | self::hl7:city | self::hl7:state | self::hl7:country | self::hl7:additionalLocator)]"
         id="d2055728e26-true-d2056404e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055728e26-true-d2056404e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:streetAddressLine | hl7:streetName | hl7:houseNumber | hl7:postalCode | hl7:city | hl7:state | hl7:country | hl7:additionalLocator (rule-reference: d2055728e26-true-d2056404e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/*[not(@xsi:nil = 'true')][not(self::hl7:name[not(@nullFlavor)] | self::hl7:name[@nullFlavor='UNK'] | self::hl7:name[@nullFlavor='MSK'])]"
         id="d2055728e30-true-d2056459e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2055728e30-true-d2056459e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:name[not(@nullFlavor)] | hl7:name[@nullFlavor='UNK'] | hl7:name[@nullFlavor='MSK'] (rule-reference: d2055728e30-true-d2056459e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:informant/hl7:relatedEntity[@classCode = 'PRS']/hl7:relatedPerson/hl7:name[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:prefix | self::hl7:family[not(@nullFlavor)] | self::hl7:given[not(@nullFlavor)] | self::hl7:suffix)]"
         id="d2056452e10-true-d2056486e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2056452e10-true-d2056486e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:prefix | hl7:family[not(@nullFlavor)] | hl7:given[not(@nullFlavor)] | hl7:suffix (rule-reference: d2056452e10-true-d2056486e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/*[not(@xsi:nil = 'true')][not(self::hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']])]"
         id="d42e48748-true-d2056540e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d42e48748-true-d2056540e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']] (rule-reference: d42e48748-true-d2056540e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | self::hl7:id[not(@nullFlavor)] | self::hl7:code | self::hl7:text | self::hl7:setId[not(@nullFlavor)] | self::hl7:versionNumber[not(@nullFlavor)])]"
         id="d2056517e6-true-d2056573e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.html"
              test="not(.)">(atcdabbr_entry_BisherigeSchwangerschaften)/d2056517e6-true-d2056573e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14'] | hl7:id[not(@nullFlavor)] | hl7:code | hl7:text | hl7:setId[not(@nullFlavor)] | hl7:versionNumber[not(@nullFlavor)] (rule-reference: d2056517e6-true-d2056573e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.46'] and hl7:templateId[@root = '2.16.840.1.113883.10.22.4.28']]/hl7:reference[hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]]/hl7:externalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.14']]/hl7:text/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d2056593e54-true-d2056605e0">
      <assert role="error"
              see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.9.1-2021-05-06T093820.html"
              test="not(.)">(atcdabrr_other_NarrativeTextReference)/d2056593e54-true-d2056605e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d2056593e54-true-d2056605e0)</assert>
   </rule>
</pattern>
