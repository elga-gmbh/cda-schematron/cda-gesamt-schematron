<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.2.2.24
Name: Empfohlene Anordnungen Pflege
Description: Empfohlene Anordnungen an die weitere Pflege
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.2.2.24-2013-09-09T000000">
   <title>Empfohlene Anordnungen Pflege</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]
Item: (EmpfohleneAnordnungenPflege)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]
Item: (EmpfohleneAnordnungenPflege)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]"
         id="d42e9487-false-d176174e0">
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']) &gt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.24'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']) &lt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:templateId[@root = '1.2.40.0.34.11.2.2.24'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')]) &gt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')]) &lt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(EmpfohleneAnordnungenPflege): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']
Item: (EmpfohleneAnordnungenPflege)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']"
         id="d42e9491-false-d176217e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(EmpfohleneAnordnungenPflege): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.2.2.24')">(EmpfohleneAnordnungenPflege): Der Wert von root MUSS '1.2.40.0.34.11.2.2.24' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')]
Item: (EmpfohleneAnordnungenPflege)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:code[(@code = '56447-6' and @codeSystem = '2.16.840.1.113883.6.1')]"
         id="d42e9498-false-d176232e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(EmpfohleneAnordnungenPflege): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="@nullFlavor or (@code='56447-6' and @codeSystem='2.16.840.1.113883.6.1')">(EmpfohleneAnordnungenPflege): Der Elementinhalt MUSS einer von 'code '56447-6' codeSystem '2.16.840.1.113883.6.1'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:title[not(@nullFlavor)]
Item: (EmpfohleneAnordnungenPflege)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:title[not(@nullFlavor)]"
         id="d42e9509-false-d176248e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(EmpfohleneAnordnungenPflege): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="text()='Empfohlene Anordnungen an die weitere Pflege'">(EmpfohleneAnordnungenPflege): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Empfohlene Anordnungen an die weitere Pflege'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.2.2.24
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:text[not(@nullFlavor)]
Item: (EmpfohleneAnordnungenPflege)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.11.2.2.24']]/hl7:text[not(@nullFlavor)]"
         id="d42e9517-false-d176262e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="http://elga.art-decor.org/elga-html-20230508T144915/tmp-1.2.40.0.34.11.2.2.24-2013-09-09T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(EmpfohleneAnordnungenPflege): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
