<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.89
Name: Device Resolution Observation
Description: Die Device Resolution Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Die Auflösung einer Messung sagt aus wie klein der Unterschied zwischen zwei Messungen sein muss, um vom Gerät erkannt zu werden. Beispielsweise kann ein Thermometer eine Auflösung von 0,1 Grad Celsius haben. Die Auflösung wird hier
                angegeben in den Einheiten der Messgröße und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Accuracy Observation-Elemente existieren. Wenn das Gerät die Daten über die Auflösung oder die anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037">
   <title>Device Resolution Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]"
         id="d41e56004-false-d3612330e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89']) &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89']) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]) &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] | hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Auswahl (hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]  oder  hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="$elmcount &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Auswahl (hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]  oder  hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89']
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89']"
         id="d41e56010-false-d3612406e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.89')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.89' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']"
         id="d41e56019-false-d3612421e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.6')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.6' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]"
         id="d41e56030-false-d3612443e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@code) = ('17441009')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von code MUSS '17441009' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.9')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.9' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@codeSystemName) = ('SNOMED CT')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="string(@displayName) = ('device measurement resolution')">(atcdabbr_entry_DeviceResolutionObservation): Der Wert von displayName MUSS 'device measurement resolution' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]/hl7:translation
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]/hl7:translation"
         id="d41e56040-false-d3612481e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@code">(atcdabbr_entry_DeviceResolutionObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceResolutionObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@codeSystem">(atcdabbr_entry_DeviceResolutionObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceResolutionObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]/ips:designation
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:code[(@code = '17441009' and @codeSystem = '2.16.840.1.113883.6.9')]/ips:designation"
         id="d41e56055-false-d3612508e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@language">(atcdabbr_entry_DeviceResolutionObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceResolutionObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]"
         id="d41e56065-false-d3612524e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceResolutionObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]"
         id="d41e56070-false-d3612543e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@value">(atcdabbr_entry_DeviceResolutionObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceResolutionObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]"
         id="d41e56078-false-d3612559e0">
      <extends rule="PQ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@value) or matches(@value, '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceResolutionObservation): @value ist keine gültige PQ Zahl <value-of select="@value"/>
      </assert>
      <let name="theUnit" value="@unit"/>
      <let name="UCUMtest"
           value="doc('include/voc-UCUM.xml')/*/ucum[@unit=$theUnit]/@message"/>
      <assert role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="$UCUMtest='OK' or string-length($UCUMtest)=0">(atcdabbr_entry_DeviceResolutionObservation): value/@unit (PQ) MUSS eine gültige UCUM-Einheit sein (<value-of select="$UCUMtest"/>).</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@nullFlavor or ($xsiLocalName='PQ' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@value">(atcdabbr_entry_DeviceResolutionObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@value) or matches(string(@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceResolutionObservation): Attribute @value ist keine gültige real Zahl <value-of select="@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@unit">(atcdabbr_entry_DeviceResolutionObservation): Attribut @unit MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="not(@unit) or (string-length(@unit) &gt; 0 and not(matches(@unit,'\s')))">(atcdabbr_entry_DeviceResolutionObservation): Attribute @unit MUSS vom Datentyp 'cs' sein  - '<value-of select="@unit"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.89
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]
Item: (atcdabbr_entry_DeviceResolutionObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.89'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.6'][@extension = '2015-08-17']]/hl7:value[resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')]"
         id="d41e56096-false-d3612586e0">
      <extends rule="ST"/>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.html"
              test="@nullFlavor or ($xsiLocalName='ST' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceResolutionObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
</pattern>
