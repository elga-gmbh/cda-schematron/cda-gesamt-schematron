<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.105
Name: Laboratory Specialty Section (Mikroskopie) - codiert
Description: Die Section "Laboratory Specialty Section (Mikroskopie)" entspricht jenem Befundbereich eines Mikrobiologiebefundes, in dem alle Mikroskopie-Ergebnisse dokumentiert werden. Jedes Ergebnis wird direkt in einer separaten "Laboratory Observation" codiert werden.  Darstellung der Ergebnisse  "section/text" enthält den narrativen Text, der der
                CDA Level 2 Darstellung der medizinischen Inhalte entspricht.  "section/text" darf keine medizinisch relevanten Inhalte enthalten, die nicht aus den CDA Level 3 codierten Daten abgeleitet werden können.  Die CDA Level 3 codierten Informationen sind über das Template "Laboratory Report Data Processing Entry" abzubilden, welches die Grundlage für die Strukturierung (Abschnitte,
                Formatierung, etc.) von "section/text" bildet.   Die Darstellung der Mikroskopieergebnisse in "section/text" hat tabellarisch zu erfolgen, wobei die Tabelle wie folgt aufgebaut sein sollte. Die Optionalität in dieser Tabelle bezieht sich auf die Da
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032">
   <title>Laboratory Specialty Section (Mikroskopie) - codiert</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]"
         id="d42e15230-false-d774446e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']) = 0">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1'] DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']"
         id="d42e15236-false-d774715e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.105')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.105' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']"
         id="d42e15244-false-d774727e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.3.2.1')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.3.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:id[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:id[not(@nullFlavor)]"
         id="d42e15257-false-d774739e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:code[(@code = '395538009' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d42e15264-false-d774750e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="@nullFlavor or (@code='395538009' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='Microscopic specimen observation (finding)')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Elementinhalt MUSS einer von 'code '395538009' codeSystem '2.16.840.1.113883.6.96' displayName='Microscopic specimen observation (finding)'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@codeSystemName) = ('SNOMED CT') or not(@codeSystemName)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:title[not(@nullFlavor)]"
         id="d42e15274-false-d774772e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="text()='Mikroskopie'">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Mikroskopie'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:text[not(@nullFlavor)]"
         id="d42e15280-false-d774786e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@typeCode) = ('DRIV')">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.105
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_LaboratorySpecialtySectionMikroskopieCodiert)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.105']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.105-2021-04-06T081032.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySectionMikroskopieCodiert): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
