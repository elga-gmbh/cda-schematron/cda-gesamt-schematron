<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.70
Name: Messergebnisse Gruppe Entry
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012">
   <title>Messergebnisse Gruppe Entry</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]"
         id="d41e44902-false-d3135643e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@classCode) = ('CLUSTER')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von classCode MUSS 'CLUSTER' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@moodCode) = ('EVN')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70']) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70']) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01']) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01']) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:id[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:id[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="count(hl7:effectiveTime) &lt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Element hl7:effectiveTime kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.71'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]] | hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.101'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.8'][@extension = '2015-11-19'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="$elmcount &gt;= 1">(atcdabbr_entry_MessergebnisseGruppeEntry): Auswahl (hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.71'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]  oder  hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.101'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.8'][@extension = '2015-11-19'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]) enthält nicht genügend Elemente [min 1x]</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70']
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70']"
         id="d41e44908-false-d3136422e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.70')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.70' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01']
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01']"
         id="d41e44917-false-d3136437e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.22.4.1')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von root MUSS '2.16.840.1.113883.10.20.22.4.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@extension) = ('2015-08-01')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von extension MUSS '2015-08-01' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_MessergebnisseGruppeEntry): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']"
         id="d41e44927-false-d3136459e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.16')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.16' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@extension) = ('2015-11-19')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von extension MUSS '2015-11-19' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_MessergebnisseGruppeEntry): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:id[not(@nullFlavor)]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:id[not(@nullFlavor)]"
         id="d41e44939-false-d3136480e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:code[(@code = '15220000' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d41e44951-false-d3136491e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="@nullFlavor or (@code='15220000' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='Laboratory test' and @codeSystemName='SNOMED-CT')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Elementinhalt MUSS einer von 'code '15220000' codeSystem '2.16.840.1.113883.6.96' displayName='Laboratory test' codeSystemName='SNOMED-CT'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:statusCode[@code = 'completed']
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:statusCode[@code = 'completed']"
         id="d41e44962-false-d3136508e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(atcdabbr_entry_MessergebnisseGruppeEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="@nullFlavor or (@code='completed')">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime">
      <let name="elmcount"
           value="count(hl7:low[@value] | hl7:low[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &gt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:low[@value]  oder  hl7:low[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:low[@value]) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:low[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:low[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:low[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
      <let name="elmcount"
           value="count(hl7:high[@value] | hl7:high[@nullFlavor='UNK'])"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &gt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält nicht genügend Elemente [min 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="$elmcount &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Auswahl (hl7:high[@value]  oder  hl7:high[@nullFlavor='UNK']) enthält zu viele Elemente [max 1x]</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:high[@value]) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:high[@value] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="count(hl7:high[@nullFlavor='UNK']) &lt;= 1">(atcdabbr_other_TimeIntervalInformationMinimal): Element hl7:high[@nullFlavor='UNK'] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:low[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:low[@value]">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:low[@nullFlavor='UNK']">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:high[@value]
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:high[@value]">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.9.15
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']
Item: (atcdabbr_other_TimeIntervalInformationMinimal)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:effectiveTime/hl7:high[@nullFlavor='UNK']">
      <extends rule="TS.AT.TZ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TS')">(atcdabbr_other_TimeIntervalInformationMinimal): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="not(*)">(atcdabbr_other_TimeIntervalInformationMinimal): <value-of select="local-name()"/> with datatype TS.AT.TZ, SHOULD NOT have child elements.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.9.15-2021-06-28T140229.html"
              test="string(@nullFlavor) = ('UNK')">(atcdabbr_other_TimeIntervalInformationMinimal): Der Wert von nullFlavor MUSS 'UNK' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.71'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.71'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.70
Context: *[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.101'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.8'][@extension = '2015-11-19'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]
Item: (atcdabbr_entry_MessergebnisseGruppeEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]]/hl7:organizer[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.70'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.1'][@extension = '2015-08-01'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.16'][@extension = '2015-11-19']]/hl7:component[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.101'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.32'][@extension = '2015-08-17'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.8'][@extension = '2015-11-19'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.22.4.2'][@extension = '2015-08-01']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.70-2018-07-25T110012.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atcdabbr_entry_MessergebnisseGruppeEntry): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
