<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.88
Name: Device Sampling Frequency Observation
Description: Die Device Sampling Frequency Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Die Abtastfrequenz oder Samplefrequenz einer Messung sagt aus wie schnell hintereinander ein Messwert gemessen werden kann. Beispielsweise kann ein Pulsmesser jede Sekunde einen Wert messen. Die Abtastfrequenz wird hier in
                Millisekunden angegeben und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Sampling Frequency Observation-Elemente existieren. Wenn das Gerät die Daten über die Abtastfrequenz oder die anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.88-2020-02-19T183534-closed">
   <title>Device Sampling Frequency Observation</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']])]"
         id="d41e55840-true-d3612185e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2020-02-19T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceSamplingFrequencyObservation)/d41e55840-true-d3612185e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']] (rule-reference: d41e55840-true-d3612185e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17'] | self::hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')] | self::hl7:text[not(@nullFlavor)] | self::hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')])]"
         id="d41e55878-true-d3612227e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2020-02-19T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceSamplingFrequencyObservation)/d41e55878-true-d3612227e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17'] | hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')] | hl7:text[not(@nullFlavor)] | hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] (rule-reference: d41e55878-true-d3612227e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]/*[not(@xsi:nil = 'true')][not(self::hl7:translation | self::ips:designation)]"
         id="d41e55904-true-d3612259e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2020-02-19T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceSamplingFrequencyObservation)/d41e55904-true-d3612259e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:translation | ips:designation (rule-reference: d41e55904-true-d3612259e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d41e55942-true-d3612283e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2020-02-19T183534.html"
              test="not(.)">(atcdabbr_entry_DeviceSamplingFrequencyObservation)/d41e55942-true-d3612283e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d41e55942-true-d3612283e0)</assert>
   </rule>
</pattern>
