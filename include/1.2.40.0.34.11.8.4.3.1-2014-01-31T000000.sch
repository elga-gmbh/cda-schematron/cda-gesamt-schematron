<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.11.8.4.3.1
Name: Medikation Pharmazeutische Empfehlung Entry eMedikation
Description: 
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000">
   <title>Medikation Pharmazeutische Empfehlung Entry eMedikation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]"
         id="d42e9198-false-d29891e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]"
         id="d42e9204-false-d30146e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@classCode) = ('OBS')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@moodCode) = ('EVN')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:supply]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:entryRelationship[@typeCode='REFR'][hl7:supply] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:entryRelationship[@typeCode='REFR'][hl7:organizer] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:entryRelationship[@typeCode='REFR'][hl7:organizer] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1']"
         id="d42e9212-false-d30224e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@root) = ('1.2.40.0.34.11.8.4.3.1')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von root MUSS '1.2.40.0.34.11.8.4.3.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']"
         id="d42e9217-false-d30239e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.9.1.3.3')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.9.1.3.3' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.4']"
         id="d42e9225-false-d30254e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@root) = ('1.2.40.0.10.1.4.3.4.2.4')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von root MUSS '1.2.40.0.10.1.4.3.4.2.4' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@extension">(MedikationPharmazeutischeEmpfehlungEntry): Attribut @extension MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]"
         id="d42e9244-false-d30275e0">
      <extends rule="CE"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <let name="theCode" value="@code"/>
      <let name="theCodeSystem" value="@codeSystem"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@nullFlavor or exists(doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1][conceptList/concept[@code = $theCode][@codeSystem = $theCodeSystem]])">(MedikationPharmazeutischeEmpfehlungEntry): Der Elementinhalt MUSS einer von '1.2.40.0.34.10.71 ELGA_MedikationPharmazeutischeEmpfehlungStatus (DYNAMIC)' sein.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:originalText) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:originalText kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/hl7:originalText
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:code[concat(@code, @codeSystem) = doc('include/voc-1.2.40.0.34.10.71-DYNAMIC.xml')//valueSet[1]/conceptList/concept/concat(@code, @codeSystem) or @nullFlavor]/hl7:originalText"
         id="d42e9254-false-d30301e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:statusCode[@code = 'completed']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:statusCode[@code = 'completed']"
         id="d42e9259-false-d30312e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@nullFlavor or (@code='completed')">(MedikationPharmazeutischeEmpfehlungEntry): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]"
         id="d42e9267-false-d30326e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('REFR')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'REFR' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]"
         id="d42e9281-false-d30343e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@classCode) = ('SBADM')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von classCode MUSS 'SBADM' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@moodCode) = ('INT')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von moodCode MUSS 'INT' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']"
         id="d42e9289-false-d30377e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@root) = ('1.2.40.0.10.1.4.3.4.2.2')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von root MUSS '1.2.40.0.10.1.4.3.4.2.2' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@extension">(MedikationPharmazeutischeEmpfehlungEntry): Attribut @extension MUSS vorkommen.</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]"
         id="d42e9306-false-d30395e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]"
         id="d42e9308-false-d30411e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:manufacturedMaterial[@nullFlavor = 'NA']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:manufacturedMaterial[@nullFlavor = 'NA'] ist required [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:manufacturedMaterial[@nullFlavor = 'NA']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:manufacturedMaterial[@nullFlavor = 'NA'] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/hl7:manufacturedMaterial[@nullFlavor = 'NA']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:substanceAdministration]/hl7:substanceAdministration[hl7:id[@root = '1.2.40.0.10.1.4.3.4.2.2']]/hl7:consumable[not(@nullFlavor)][hl7:manufacturedProduct]/hl7:manufacturedProduct[not(@nullFlavor)][hl7:manufacturedMaterial[@nullFlavor = 'NA']]/hl7:manufacturedMaterial[@nullFlavor = 'NA']"
         id="d42e9310-false-d30427e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@nullFlavor) = ('NA')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von nullFlavor MUSS 'NA' sein. Gefunden: "<value-of select="@nullFlavor"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:supply]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:supply]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('REFR')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'REFR' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]"
         id="d42e9328-false-d30449e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('REFR')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'REFR' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@inversionInd) = ('false')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von inversionInd MUSS 'false' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:organizer[hl7:statusCode[@code = 'completed']]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:organizer[hl7:statusCode[@code = 'completed']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:organizer[hl7:statusCode[@code = 'completed']]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:organizer[hl7:statusCode[@code = 'completed']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]"
         id="d42e9337-false-d30581e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@classCode) = ('CLUSTER')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von classCode MUSS 'CLUSTER' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@moodCode) = ('EVN')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:statusCode[@code = 'completed']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:statusCode[@code = 'completed']"
         id="d42e9345-false-d30672e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@nullFlavor or (@code='completed')">(MedikationPharmazeutischeEmpfehlungEntry): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:seperatableInd[not(@nullFlavor)]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:seperatableInd[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:seperatableInd[not(@nullFlavor)]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:seperatableInd[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:seperatableInd[not(@nullFlavor)]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[hl7:substanceAdministration[hl7:templateId[@root = '1.2.40.0.34.11.8.1.3.1'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.1.24'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.4.7'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.6']]]/hl7:seperatableInd[not(@nullFlavor)]">
      <extends rule="BL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BL')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:BL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]"
         id="d42e9362-false-d30777e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('REFR')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'REFR' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@inversionInd) = ('false')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von inversionInd MUSS 'false' sein. Gefunden: "<value-of select="@inversionInd"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:organizer[hl7:statusCode[@code = 'completed']]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:organizer[hl7:statusCode[@code = 'completed']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:organizer[hl7:statusCode[@code = 'completed']]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:organizer[hl7:statusCode[@code = 'completed']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]"
         id="d42e9371-false-d30803e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@classCode) = ('CLUSTER')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von classCode MUSS 'CLUSTER' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@moodCode) = ('EVN')">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:statusCode[@code = 'completed']) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:statusCode[@code = 'completed'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:component[not(@nullFlavor)]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:component[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:component[not(@nullFlavor)]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:component[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:statusCode[@code = 'completed']
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:statusCode[@code = 'completed']"
         id="d42e9379-false-d30837e0">
      <extends rule="CS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CS')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CS" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="@nullFlavor or (@code='completed')">(MedikationPharmazeutischeEmpfehlungEntry): Der Elementinhalt MUSS einer von 'code 'completed'' sein.</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[not(@nullFlavor)]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[not(@nullFlavor)]">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(MedikationPharmazeutischeEmpfehlungEntry): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:seperatableInd[not(@nullFlavor)]) &gt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:seperatableInd[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="count(hl7:seperatableInd[not(@nullFlavor)]) &lt;= 1">(MedikationPharmazeutischeEmpfehlungEntry): Element hl7:seperatableInd[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.11.8.4.3.1
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[not(@nullFlavor)]/hl7:seperatableInd[not(@nullFlavor)]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
-->
   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[not(@nullFlavor)]/hl7:seperatableInd[not(@nullFlavor)]">
      <extends rule="BL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.11.8.4.3.1-2014-01-31T000000.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'BL')">(MedikationPharmazeutischeEmpfehlungEntry): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:BL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: elgaambbef-d42e9384
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.11.8.4.3.1'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.9.1.3.3']]/hl7:entryRelationship[@typeCode='REFR'][hl7:organizer]/hl7:organizer[hl7:statusCode[@code = 'completed']]/hl7:component[not(@nullFlavor)]
Item: (MedikationPharmazeutischeEmpfehlungEntry)
--></pattern>
