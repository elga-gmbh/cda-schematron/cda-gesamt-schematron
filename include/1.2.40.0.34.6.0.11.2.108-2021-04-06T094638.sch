<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.2.108
Name: Laboratory Specialty Section (Infektionsserologie)
Description: Die "Laboratory Specialty Section (Infektionsserologie)" entspricht jenem Befundbereich eines Mikrobiologiebefundes, in dem alle Ergebnisse der Infektionsserologie dokumentiert werden. Jedes ermittelte Ergebnis wird direkt in einer separaten "Laboratory Observation" codiert.  Darstellung der Ergebnisse  "section/text" enthält den
                narrativen Text, der der CDA Level 2 Darstellung der medizinischen Inhalte entspricht.  "section/text" darf keine medizinisch relevanten Inhalte enthalten, die nicht aus den CDA Level 3 codierten Daten abgeleitet werden können.  Die CDA Level 3 codierten Informationen sind über das Template "Laboratory Report Data Processing Entry" abzubilden, welches die Grundlage für die
                Strukturierung (Abschnitte, Formatierung, etc.) von "section/text" bildet.   Die Darstellung der Infektionsserologie in "section/text" hat tabellarisch zu erfolgen, wobei die Tabelle wie folgt aufgebaut sein sollte. Die Optionalität in dieser Tabelle bezieht s
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638">
   <title>Laboratory Specialty Section (Infektionsserologie)</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]"
         id="d42e16225-false-d990302e0">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@classCode) = ('DOCSECT') or not(@classCode)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von classCode MUSS 'DOCSECT' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@moodCode) = ('EVN') or not(@moodCode)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von moodCode MUSS 'EVN' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']) &gt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']) = 0">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1'] DARF NICHT vorkommen.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:id[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:id[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:title[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:title[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:title[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:title[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &gt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="count(hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]) &lt;= 1">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Element hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']"
         id="d42e16231-false-d990571e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.2.108')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von root MUSS '1.2.40.0.34.6.0.11.2.108' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:templateId[@root='1.3.6.1.4.1.19376.1.3.3.2.1']"
         id="d42e16239-false-d990583e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@root) = ('1.3.6.1.4.1.19376.1.3.3.2.1')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von root MUSS '1.3.6.1.4.1.19376.1.3.3.2.1' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:id[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:id[not(@nullFlavor)]"
         id="d42e16252-false-d990595e0">
      <extends rule="II"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:code[(@code = '722143004' and @codeSystem = '2.16.840.1.113883.6.96')]"
         id="d42e16259-false-d990606e0">
      <extends rule="CE"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CE')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CE" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="@nullFlavor or (@code='722143004' and @codeSystem='2.16.840.1.113883.6.96' and @displayName='Infectious disease diagnostic study note (record artifact)')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Elementinhalt MUSS einer von 'code '722143004' codeSystem '2.16.840.1.113883.6.96' displayName='Infectious disease diagnostic study note (record artifact)'' sein.</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@codeSystemName) = ('SNOMED CT') or not(@codeSystemName)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von codeSystemName MUSS 'SNOMED CT' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:title[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:title[not(@nullFlavor)]"
         id="d42e16269-false-d990628e0">
      <extends rule="ST"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="text()='Infektionsserologie'">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Elementinhalt von 'hl7:title[not(@nullFlavor)]' MUSS ''Infektionsserologie'' sein. Gefunden: "<value-of select="."/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:text[not(@nullFlavor)]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->

   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:text[not(@nullFlavor)]"
         id="d42e16275-false-d990642e0">
      <extends rule="SD.TEXT"/>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'SD.TEXT')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:SD.TEXT" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:entry[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.25'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.3.1']]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@typeCode) = ('DRIV')">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von typeCode MUSS 'DRIV' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.2.108
Context: *[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]
Item: (atlab_section_LaboratorySpecialtySectionInfektionsserologie)
-->
   <rule fpi="RULC-1"
         context="*[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]]/hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.108']]/hl7:component[hl7:section[hl7:templateId[@root = '1.2.40.0.34.6.0.11.2.8']]]">
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@typeCode) = ('COMP') or not(@typeCode)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von typeCode MUSS 'COMP' sein. Gefunden: "<value-of select="@typeCode"/>"</assert>
      <assert role="error"
              see="https://elga.art-decor.org/at-lab-html-20250116T124017/tmp-1.2.40.0.34.6.0.11.2.108-2021-04-06T094638.html"
              test="string(@contextConductionInd) = ('true') or not(@contextConductionInd)">(atlab_section_LaboratorySpecialtySectionInfektionsserologie): Der Wert von contextConductionInd MUSS 'true' sein. Gefunden: "<value-of select="@contextConductionInd"/>"</assert>
   </rule>
</pattern>
