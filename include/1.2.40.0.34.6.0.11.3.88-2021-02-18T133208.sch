<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.88
Name: Device Sampling Frequency Observation
Description: Die Device Sampling Frequency Observation kann mit einem oder mehreren Elementen in einem Device Information Organizer vorhanden sein. Die Abtastfrequenz oder Samplefrequenz einer Messung sagt aus wie schnell hintereinander ein Messwert gemessen werden kann. Beispielsweise kann ein Pulsmesser jede Sekunde einen Wert messen. Die Abtastfrequenz wird hier in
                Millisekunden angegeben und weil ein Gerät mehr als nur eine Messgröße messen kann, können mehrere Device Sampling Frequency Observation-Elemente existieren. Wenn das Gerät die Daten über die Abtastfrequenz oder die anderen Geräte-Details nicht zurückgibt, können diese, wenn gewünscht, auch manuell oder über andere Wege eingetragen werden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208">
   <title>Device Sampling Frequency Observation</title>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]"
         id="d41e55754-false-d3611533e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@classCode) = ('OBS')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von classCode MUSS 'OBS' sein. Gefunden: "<value-of select="@classCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@moodCode) = ('DEF')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von moodCode MUSS 'DEF' sein. Gefunden: "<value-of select="@moodCode"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88']) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88']) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17'] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17'] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:text[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:text[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:text[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:text[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88']
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88']"
         id="d41e55760-false-d3611593e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@root) = ('1.2.40.0.34.6.0.11.3.88')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von root MUSS '1.2.40.0.34.6.0.11.3.88' sein. Gefunden: "<value-of select="@root"/>"</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']"
         id="d41e55769-false-d3611608e0">
      <extends rule="II"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'II')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:II" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@root) = ('2.16.840.1.113883.10.20.36.10')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von root MUSS '2.16.840.1.113883.10.20.36.10' sein. Gefunden: "<value-of select="@root"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@extension) = ('2015-08-17')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von extension MUSS '2015-08-17' sein. Gefunden: "<value-of select="@extension"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@extension) or string-length(@extension)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @extension MUSS vom Datentyp 'st' sein  - '<value-of select="@extension"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]"
         id="d41e55780-false-d3611630e0">
      <extends rule="CD.IPS"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@code) = ('67981')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von code MUSS '67981' sein. Gefunden: "<value-of select="@code"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@codeSystem) = ('2.16.840.1.113883.6.24')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von codeSystem MUSS '2.16.840.1.113883.6.24' sein. Gefunden: "<value-of select="@codeSystem"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@codeSystemName) = ('MDC')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von codeSystemName MUSS 'MDC' sein. Gefunden: "<value-of select="@codeSystemName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@displayName) = ('MDC_ATTR_TIME_PD_SAMP: Frequency that the device sends measurements')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von displayName MUSS 'MDC_ATTR_TIME_PD_SAMP: Frequency that the device sends measurements' sein. Gefunden: "<value-of select="@displayName"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]/hl7:translation"
         id="d41e55794-false-d3611668e0">
      <extends rule="CD"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'CD')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:CD" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@code">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribut @code MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@code) or (string-length(@code) &gt; 0 and not(matches(@code,'\s')))">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @code MUSS vom Datentyp 'cs' sein  - '<value-of select="@code"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@codeSystem">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribut @codeSystem MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@codeSystem) or matches(@codeSystem,'^[0-2](\.(0|[1-9]\d*))*$') or matches(@codeSystem,'^[A-Fa-f\d]{8}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{4}-[A-Fa-f\d]{12}$') or matches(@codeSystem,'^[A-Za-z][A-Za-z\d\-]*$')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @codeSystem MUSS vom Datentyp 'uid' sein  - '<value-of select="@codeSystem"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@codeSystemName) or string-length(@codeSystemName)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @codeSystemName MUSS vom Datentyp 'st' sein  - '<value-of select="@codeSystemName"/>'</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@displayName) or string-length(@displayName)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @displayName MUSS vom Datentyp 'st' sein  - '<value-of select="@displayName"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:code[(@code = '67981' and @codeSystem = '2.16.840.1.113883.6.24')]/ips:designation"
         id="d41e55808-false-d3611695e0">
      <extends rule="ST"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ST')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ST" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@language">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribut @language MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@language) or (string-length(@language) &gt; 0 and not(matches(@language,'\s')))">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @language MUSS vom Datentyp 'cs' sein  - '<value-of select="@language"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]"
         id="d41e55818-false-d3611711e0">
      <extends rule="ED"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'ED')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:ED" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:reference[not(@nullFlavor)]) &gt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:reference[not(@nullFlavor)] ist mandatory [min 1x].</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="count(hl7:reference[not(@nullFlavor)]) &lt;= 1">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Element hl7:reference[not(@nullFlavor)] kommt zu häufig vor [max 1x].</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/hl7:reference[not(@nullFlavor)]"
         id="d41e55823-false-d3611730e0">
      <extends rule="TEL"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="empty(@xsi:type) or resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'TEL')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:TEL" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@value">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@value) or string-length(@value)&gt;0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @value MUSS vom Datentyp 'st' sein  - '<value-of select="@value"/>'</assert>
   </rule>
   <!--
Template derived rules for ID: 1.2.40.0.34.6.0.11.3.88
Context: *[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]
Item: (atcdabbr_entry_DeviceSamplingFrequencyObservation)
-->

   <rule fpi="RULC-1"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.88'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.10'][@extension = '2015-08-17']]/hl7:value[not(@nullFlavor)][resolve-QName(@xsi:type, .) = QName('urn:hl7-org:v3', 'PQ')]"
         id="d41e55829-false-d3611746e0">
      <extends rule="PQ"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@value) or matches(@value, '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): @value ist keine gültige PQ Zahl <value-of select="@value"/>
      </assert>
      <let name="theUnit" value="@unit"/>
      <let name="UCUMtest"
           value="doc('include/voc-UCUM.xml')/*/ucum[@unit=$theUnit]/@message"/>
      <assert role="warning"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="$UCUMtest='OK' or string-length($UCUMtest)=0">(atcdabbr_entry_DeviceSamplingFrequencyObservation): value/@unit (PQ) MUSS eine gültige UCUM-Einheit sein (<value-of select="$UCUMtest"/>).</assert>
      <let name="xsiLocalName"
           value="if (contains(@xsi:type, ':')) then substring-after(@xsi:type,':') else @xsi:type"/>
      <let name="xsiLocalNS"
           value="if (contains(@xsi:type, ':')) then namespace-uri-for-prefix(substring-before(@xsi:type,':'),.) else namespace-uri-for-prefix('',.)"/>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@nullFlavor or ($xsiLocalName='PQ' and $xsiLocalNS='urn:hl7-org:v3')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Wenn eine @xsi:type Instruktion anwesend ist MUSS diese den Wert "{urn:hl7-org:v3}:PQ" haben. Gefunden "{<value-of select="namespace-uri-from-QName(resolve-QName(@xsi:type,.))"/>}:<value-of select="local-name-from-QName(resolve-QName(@xsi:type,.))"/>"</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="@value">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribut @value MUSS vorkommen.</assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="not(@value) or matches(string(@value), '^[-+]?\d*\.?[0-9]+([eE][-+]?\d+)?$')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Attribute @value ist keine gültige real Zahl <value-of select="@value"/>
      </assert>
      <assert role="error"
              see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.html"
              test="string(@unit) = ('ms')">(atcdabbr_entry_DeviceSamplingFrequencyObservation): Der Wert von unit MUSS 'ms' sein. Gefunden: "<value-of select="@unit"/>"</assert>
   </rule>
</pattern>
