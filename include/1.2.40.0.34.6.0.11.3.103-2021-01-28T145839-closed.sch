<?xml version="1.0" encoding="UTF-8"?>
<!--
Template derived pattern
===========================================
ID: 1.2.40.0.34.6.0.11.3.103
Name: Serienmessungs-Werte Entry
Description: Das Serienmessungs-Werte Entry dokumentiert die kontinuierlichen Messwerte in einem SLIST_PQ Datentyp. Dieses Element ist Teil einer Serienmessungs-Gruppe Entry neben der zeitliche Komponente (Start und Zeitraum zwischen den Messungen) der kontinuierlichen Messung. Wenn die Serienmessung von einem IEEE 11073 kompatiblen Gerät empfangen wird, sind die Messungen im
                Real Time Sample Array (RTSA) metric objects zu finden.
-->
<pattern xmlns="http://purl.oclc.org/dsdl/schematron"
         id="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839-closed">
   <title>Serienmessungs-Werte Entry</title>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/*[not(@xsi:nil = 'true')][not(self::hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']])]"
         id="d42e56300-true-d5176373e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56300-true-d5176373e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']] (rule-reference: d42e56300-true-d5176373e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/*[not(@xsi:nil = 'true')][not(self::hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | self::hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | self::hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | self::hl7:text[not(@nullFlavor)] | self::hl7:statusCode[@code = 'completed'] | self::hl7:value)]"
         id="d42e56342-true-d5176422e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56342-true-d5176422e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] | hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17'] | hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']] | hl7:text[not(@nullFlavor)] | hl7:statusCode[@code = 'completed'] | hl7:value (rule-reference: d42e56342-true-d5176422e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/*[not(@xsi:nil = 'true')][not(self::hl7:originalText[not(@nullFlavor)] | self::hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | self::hl7:translation | self::ips:designation)]"
         id="d42e56366-true-d5176466e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56366-true-d5176466e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:originalText[not(@nullFlavor)] | hl7:translation[@codeSystem = '2.16.840.1.113883.6.24'] | hl7:translation | ips:designation (rule-reference: d42e56366-true-d5176466e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:code[hl7:translation[@codeSystem = '2.16.840.1.113883.6.24']]/hl7:originalText[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e56383-true-d5176480e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56383-true-d5176480e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e56383-true-d5176480e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:text[not(@nullFlavor)]/*[not(@xsi:nil = 'true')][not(self::hl7:reference[not(@nullFlavor)])]"
         id="d42e56437-true-d5176515e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56437-true-d5176515e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:reference[not(@nullFlavor)] (rule-reference: d42e56437-true-d5176515e0)</assert>
   </rule>

   <!-- Checking undefined contents for template/element @isClosed="true". Match context that we did not already match -->
   <rule fpi="RUL-QQQ"
         context="*[hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]]/hl7:observation[hl7:templateId[@root = '1.2.40.0.34.6.0.11.3.103'] and hl7:templateId[@root = '2.16.840.1.113883.10.20.36.36'][@extension = '2015-08-17']]/hl7:value/*[not(@xsi:nil = 'true')][not(self::hl7:origin[not(@nullFlavor)] | self::hl7:scale[not(@nullFlavor)] | self::hl7:digits[not(@nullFlavor)])]"
         id="d42e56454-true-d5176550e0">
      <assert role="error"
              see="http://elga.art-decor.org/elgaambbef-html-20230118T144941/tmp-1.2.40.0.34.6.0.11.3.103-2021-01-28T145839.html"
              test="not(.)">(SerienmessungsWerteEntry)/d42e56454-true-d5176550e0: '<value-of select="name(..)"/>' ist als geschlossen definiert und dieses Element '<name/>' ist nicht erlaubt oder nicht mit diesem Inhalt erlaubt. Dies kann templateId, code oder id betreffen. Erwartet:  hl7:origin[not(@nullFlavor)] | hl7:scale[not(@nullFlavor)] | hl7:digits[not(@nullFlavor)] (rule-reference: d42e56454-true-d5176550e0)</assert>
   </rule>
</pattern>
