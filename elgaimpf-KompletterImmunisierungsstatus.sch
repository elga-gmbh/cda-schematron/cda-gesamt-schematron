<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Kompletter Immunisierungsstatus v2 (1.2.40.0.34.777.4.4.18 2023-05-09T07:35:05)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:hl7-org:sdtc" prefix="sdtc"/>
   <ns uri="urn:hl7-at:v3" prefix="hl7at"/>
   <ns uri="urn:hl7-org:ips" prefix="ips"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <!-- Include realm specific schematron -->
   <!-- Include scenario label -->
   <let name="scenariolabel" value="'KompletterImmunisierungsstatus'"/>
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario KompletterImmunisierungsstatus -->

   <!-- eimpf_document_KompletterImmunisierungsstatus -->
   <pattern>
      <title>eimpf_document_KompletterImmunisierungsstatus</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]"
                 see="http://elga.art-decor.org/elgaimpf-html-20250116T125121/tmp-1.2.40.0.34.6.0.11.0.4-2023-01-24T145934.html">(eimpf_document_KompletterImmunisierungsstatus): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.19.2'] and hl7:templateId[@root = '1.3.6.1.4.1.19376.1.5.3.1.1.18.1.2']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.6.0.11.0.4-2023-01-24T145934.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.0.4-2023-01-24T145934-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.4-2023-01-24T145934"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.2-2025-01-07T114526"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.4-2023-04-04T160843"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.5-2023-05-02T125810"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.7-2023-04-05T141600"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.15-2023-03-30T083406"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.16-2023-03-28T152725"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.17-2023-04-03T135056"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.18-2021-08-20T113823"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.20-2023-04-13T111348"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.21-2023-03-30T083827"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.3-2024-11-15T083939"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.8-2023-03-31T121819"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.9-2023-03-30T084635"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.28-2021-02-19T133632"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446"/>
   </phase>
   <phase id="eimpf_document_KompletterImmunisierungsstatus">
      <active pattern="template-1.2.40.0.34.6.0.11.0.4-2023-01-24T145934"/>
   </phase>
   <phase id="eimpf_document_KompletterImmunisierungsstatus-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.4-2023-01-24T145934-closed"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed"/>
   </phase>
   <phase id="atcdabrr_section_ImpfempfehlungenKodiert-20250107T114526">
      <active pattern="template-1.2.40.0.34.6.0.11.2.2-2025-01-07T114526"/>
   </phase>
   <phase id="atcdabrr_section_ImpfempfehlungenKodiert-20250107T114526-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.2-2025-01-07T114526-closed"/>
   </phase>
   <phase id="atcdabrr_section_IndikationsgruppenKodiert">
      <active pattern="template-1.2.40.0.34.6.0.11.2.4-2023-04-04T160843"/>
   </phase>
   <phase id="atcdabrr_section_IndikationsgruppenKodiert-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.4-2023-04-04T160843-closed"/>
   </phase>
   <phase id="eimpf_section_ImpfrelevanteErkrankungen-20230502T125810">
      <active pattern="template-1.2.40.0.34.6.0.11.2.5-2023-05-02T125810"/>
   </phase>
   <phase id="eimpf_section_ImpfrelevanteErkrankungen-20230502T125810-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.5-2023-05-02T125810-closed"/>
   </phase>
   <phase id="eimpf_section_AntikoerperBestimmungKodiert-20230405T141600">
      <active pattern="template-1.2.40.0.34.6.0.11.2.7-2023-04-05T141600"/>
   </phase>
   <phase id="eimpf_section_AntikoerperBestimmungKodiert-20230405T141600-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.7-2023-04-05T141600-closed"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20241105T143545">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20241105T143545-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20250107T115116">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20250107T115116-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116-closed"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungDataProcessing-20230330T083406">
      <active pattern="template-1.2.40.0.34.6.0.11.3.15-2023-03-30T083406"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungDataProcessing-20230330T083406-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.15-2023-03-30T083406-closed"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungLaboratoryObservation-20230328T152725">
      <active pattern="template-1.2.40.0.34.6.0.11.3.16-2023-03-28T152725"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungLaboratoryObservation-20230328T152725-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.16-2023-03-28T152725-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20230509T163856">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20230509T163856-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856-closed"/>
   </phase>
   <phase id="atcdabrr_entry_Comment_single_author_informant">
      <active pattern="template-1.2.40.0.34.6.0.11.3.17-2023-04-03T135056"/>
   </phase>
   <phase id="atcdabrr_entry_Comment_single_author_informant-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.17-2023-04-03T135056-closed"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungBatteryOrganizer-20210820T113823">
      <active pattern="template-1.2.40.0.34.6.0.11.3.18-2021-08-20T113823"/>
   </phase>
   <phase id="eimpf_entry_AntikoerperBestimmungBatteryOrganizer-20210820T113823-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.18-2021-08-20T113823-closed"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20230606T145012">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20230606T145012-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012-closed"/>
   </phase>
   <phase id="atcdabbr_entry_IndikationsgruppeProblemConcern">
      <active pattern="template-1.2.40.0.34.6.0.11.3.20-2023-04-13T111348"/>
   </phase>
   <phase id="atcdabbr_entry_IndikationsgruppeProblemConcern-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.20-2023-04-13T111348-closed"/>
   </phase>
   <phase id="atcdabbr_entry_IndikationsgruppeProblem">
      <active pattern="template-1.2.40.0.34.6.0.11.3.21-2023-03-30T083827"/>
   </phase>
   <phase id="atcdabbr_entry_IndikationsgruppeProblem-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.21-2023-03-30T083827-closed"/>
   </phase>
   <phase id="atcdabbr_entry_impfplan">
      <active pattern="template-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616"/>
   </phase>
   <phase id="atcdabbr_entry_impfplan-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.22-2023-01-23T155616-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20241115T083802">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20241115T083802-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationRecommendation-20241115T083939">
      <active pattern="template-1.2.40.0.34.6.0.11.3.3-2024-11-15T083939"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationRecommendation-20241115T083939-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.3-2024-11-15T083939-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20230407T103811">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20230407T103811-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811-closed"/>
   </phase>
   <phase id="eimpf_entry_ImpfrelevanteErkrankungenProblemConcern-20230331T121819">
      <active pattern="template-1.2.40.0.34.6.0.11.3.8-2023-03-31T121819"/>
   </phase>
   <phase id="eimpf_entry_ImpfrelevanteErkrankungenProblemConcern-20230331T121819-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.8-2023-03-31T121819-closed"/>
   </phase>
   <phase id="eimpf_entry_ImpfrelevanteErkrankungProblemEntry-20230330T084635">
      <active pattern="template-1.2.40.0.34.6.0.11.3.9-2023-03-30T084635"/>
   </phase>
   <phase id="eimpf_entry_ImpfrelevanteErkrankungProblemEntry-20230330T084635-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.9-2023-03-30T084635-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyLaboratory-20210219T133632">
      <active pattern="template-1.2.40.0.34.6.0.11.9.28-2021-02-19T133632"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyLaboratory-20210219T133632-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.28-2021-02-19T133632-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20230405T101745">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20230405T101745-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier-20230407T105453">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier-20230407T105453-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor-20230404T103910">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor-20230404T103910-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer-20230228T124446">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer-20230228T124446-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446-closed"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- atcdabrr_section_ImpfungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed.sch"/>
   <!-- atcdabrr_section_ImpfempfehlungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.2-2025-01-07T114526.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.2-2025-01-07T114526-closed.sch"/>
   <!-- atcdabrr_section_IndikationsgruppenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.4-2023-04-04T160843.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.4-2023-04-04T160843-closed.sch"/>
   <!-- eimpf_section_ImpfrelevanteErkrankungen -->
   <include href="include/1.2.40.0.34.6.0.11.2.5-2023-05-02T125810.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.5-2023-05-02T125810-closed.sch"/>
   <!-- eimpf_section_AntikoerperBestimmungKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.7-2023-04-05T141600.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.7-2023-04-05T141600-closed.sch"/>
   <!-- atcdabbr_section_Beilagen -->
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed.sch"/>
   <!-- atcdabbr_section_Uebersetzung -->
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed.sch"/>
   <!-- atcdabbr_entry_Immunization -->
   <include href="include/1.2.40.0.34.6.0.11.3.1-2024-11-05T143545.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.1-2024-11-05T143545-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationSchedule -->
   <include href="include/1.2.40.0.34.6.0.11.3.10-2025-01-07T115116.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.10-2025-01-07T115116-closed.sch"/>
   <!-- atcdabbr_entry_externalDocument -->
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed.sch"/>
   <!-- eimpf_entry_AntikoerperBestimmungDataProcessing -->
   <include href="include/1.2.40.0.34.6.0.11.3.15-2023-03-30T083406.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.15-2023-03-30T083406-closed.sch"/>
   <!-- eimpf_entry_AntikoerperBestimmungLaboratoryObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.16-2023-03-28T152725.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.16-2023-03-28T152725-closed.sch"/>
   <!-- atcdabbr_entry_Zusatzklassifikation -->
   <include href="include/1.2.40.0.34.6.0.11.3.168-2023-05-09T163856.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.168-2023-05-09T163856-closed.sch"/>
   <!-- atcdabrr_entry_Comment_single_author_informant -->
   <include href="include/1.2.40.0.34.6.0.11.3.17-2023-04-03T135056.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.17-2023-04-03T135056-closed.sch"/>
   <!-- eimpf_entry_AntikoerperBestimmungBatteryOrganizer -->
   <include href="include/1.2.40.0.34.6.0.11.3.18-2021-08-20T113823.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.18-2021-08-20T113823-closed.sch"/>
   <!-- atcdabbr_entry_EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationTarget -->
   <include href="include/1.2.40.0.34.6.0.11.3.2-2023-06-06T145012.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.2-2023-06-06T145012-closed.sch"/>
   <!-- atcdabbr_entry_IndikationsgruppeProblemConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.20-2023-04-13T111348.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.20-2023-04-13T111348-closed.sch"/>
   <!-- atcdabbr_entry_IndikationsgruppeProblem -->
   <include href="include/1.2.40.0.34.6.0.11.3.21-2023-03-30T083827.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.21-2023-03-30T083827-closed.sch"/>
   <!-- atcdabbr_entry_impfplan -->
   <include href="include/1.2.40.0.34.6.0.11.3.22-2023-01-23T155616.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.22-2023-01-23T155616-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationImpfungNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.3.28-2024-11-15T083802.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.28-2024-11-15T083802-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationRecommendation -->
   <include href="include/1.2.40.0.34.6.0.11.3.3-2024-11-15T083939.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.3-2024-11-15T083939-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationBillability -->
   <include href="include/1.2.40.0.34.6.0.11.3.5-2023-04-07T103811.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.5-2023-04-07T103811-closed.sch"/>
   <!-- eimpf_entry_ImpfrelevanteErkrankungenProblemConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.8-2023-03-31T121819.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.8-2023-03-31T121819-closed.sch"/>
   <!-- eimpf_entry_ImpfrelevanteErkrankungProblemEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.9-2023-03-30T084635.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.9-2023-03-30T084635-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyTranscriber -->
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed.sch"/>
   <!-- atcdabbr_other_PerformerBody -->
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed.sch"/>
   <!-- atcdabbr_other_PerformerBodyImpfendePerson -->
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed.sch"/>
   <!-- atcdabbr_other_PerformerBodyLaboratory -->
   <include href="include/1.2.40.0.34.6.0.11.9.28-2021-02-19T133632.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.28-2021-02-19T133632-closed.sch"/>
   <!-- atcdabbr_other_vaccineProductNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed.sch"/>
   <!-- atcdabbr_other_vaccineProduct -->
   <include href="include/1.2.40.0.34.6.0.11.9.32-2023-04-05T101745.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.32-2023-04-05T101745-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyVerifier -->
   <include href="include/1.2.40.0.34.6.0.11.9.44-2023-04-07T105453.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.44-2023-04-07T105453-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyAuthorizedEditor -->
   <include href="include/1.2.40.0.34.6.0.11.9.46-2023-04-04T103910.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.46-2023-04-04T103910-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyDataEnterer -->
   <include href="include/1.2.40.0.34.6.0.11.9.47-2023-02-28T124446.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.47-2023-02-28T124446-closed.sch"/>

</schema>
<!-- versionLabel="10.5.0+20250116-2.0.0" -->
