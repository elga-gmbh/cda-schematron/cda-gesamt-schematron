<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Telemonitoring Episodenbericht v1 (1.2.40.0.34.777.3.4.15 2020-06-05T08:16:29)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:hl7-at:v3" prefix="hl7at"/>
   <ns uri="urn:hl7-org:ips" prefix="ips"/>
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <ns uri="urn:hl7-org:sdtc" prefix="sdtc"/>
   <!-- Include realm specific schematron -->
   <!-- Include scenario label -->
   <let name="scenariolabel" value="'Telemonitoring-Episodenbericht'"/>
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario Telemonitoring-Episodenbericht -->

   <!-- TGDBefund -->
   <pattern>
      <title>TGDBefund</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.23.1'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.10']]"
                 see="http://elga.art-decor.org/elgatgd-html-20240709T111220/tmp-1.2.40.0.34.6.0.11.0.10-2021-04-22T062414.html">(TGDBefund): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.23.1'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.10']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.6.0.11.0.10-2021-04-22T062414.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.0.10-2021-04-22T062414-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.10-2021-04-22T062414"/>
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.79-2021-02-18T131354"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.80-2021-02-18T131654"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.81-2021-02-18T131527"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.82-2021-02-18T131630"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.91-2021-02-18T131229"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.92-2021-02-19T114404"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.95-2021-09-01T102402"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.101-2021-01-21T081221"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.70-2021-02-18T133433"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.71-2021-02-18T133403"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.73-2021-08-31T132617"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.74-2021-02-18T132434"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.75-2021-02-18T132455"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.76-2021-02-18T132618"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.77-2021-02-18T132716"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.78-2021-02-18T132809"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.79-2021-02-18T132845"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.80-2021-02-18T133255"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.82-2021-02-18T133230"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.84-2021-02-18T132912"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.87-2021-02-18T132404"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.90-2021-02-18T132741"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.43-2022-08-24T144823"/>
   </phase>
   <phase id="TGDBefund">
      <active pattern="template-1.2.40.0.34.6.0.11.0.10-2021-04-22T062414"/>
   </phase>
   <phase id="TGDBefund-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.10-2021-04-22T062414-closed"/>
   </phase>
   <phase id="MedikationVerordnungEntryemed-20140901T000000">
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
   </phase>
   <phase id="MedikationAbgabeEntryemed-20140910T000000">
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
   </phase>
   <phase id="atcdabrr_section_VitalparameterKodiert-20210219T120303">
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303"/>
   </phase>
   <phase id="atcdabrr_section_VitalparameterKodiert-20210219T120303-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.46-2021-02-19T120303-closed"/>
   </phase>
   <phase id="atcdabbr_section_MedikationslistePSKodiert-20210219T115029">
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029"/>
   </phase>
   <phase id="atcdabbr_section_MedikationslistePSKodiert-20210219T115029-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.63-2021-02-19T115029-closed"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed"/>
   </phase>
   <phase id="atcdabbr_section_BehandlungsgrundUnkodiert-20210218T131354">
      <active pattern="template-1.2.40.0.34.6.0.11.2.79-2021-02-18T131354"/>
   </phase>
   <phase id="atcdabbr_section_BehandlungsgrundUnkodiert-20210218T131354-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.79-2021-02-18T131354-closed"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed"/>
   </phase>
   <phase id="atcdabbr_section_ZusammenfassungBehandlungUnkodiert-20210218T131654">
      <active pattern="template-1.2.40.0.34.6.0.11.2.80-2021-02-18T131654"/>
   </phase>
   <phase id="atcdabbr_section_ZusammenfassungBehandlungUnkodiert-20210218T131654-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.80-2021-02-18T131654-closed"/>
   </phase>
   <phase id="atcdabbr_section_ErhobeneDatenKodiert-20210218T131527">
      <active pattern="template-1.2.40.0.34.6.0.11.2.81-2021-02-18T131527"/>
   </phase>
   <phase id="atcdabbr_section_ErhobeneDatenKodiert-20210218T131527-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.81-2021-02-18T131527-closed"/>
   </phase>
   <phase id="atcdabbr_section_VerwendeteGeraeteKodiert-20210218T131630">
      <active pattern="template-1.2.40.0.34.6.0.11.2.82-2021-02-18T131630"/>
   </phase>
   <phase id="atcdabbr_section_VerwendeteGeraeteKodiert-20210218T131630-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.82-2021-02-18T131630-closed"/>
   </phase>
   <phase id="atcdabbr_section_AuszuegeAusErhobeneDatenUnkodiert-20210218T131229">
      <active pattern="template-1.2.40.0.34.6.0.11.2.91-2021-02-18T131229"/>
   </phase>
   <phase id="atcdabbr_section_AuszuegeAusErhobeneDatenUnkodiert-20210218T131229-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.91-2021-02-18T131229-closed"/>
   </phase>
   <phase id="BodySubSectionUnkodiertGenerisch-20210219T114404">
      <active pattern="template-1.2.40.0.34.6.0.11.2.92-2021-02-19T114404"/>
   </phase>
   <phase id="BodySubSectionUnkodiertGenerisch-20210219T114404-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.92-2021-02-19T114404-closed"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeKodiert-20210219T114416">
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416"/>
   </phase>
   <phase id="atcdabbr_section_DokumentierteEinnahmeKodiert-20210219T114416-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.94-2021-02-19T114416-closed"/>
   </phase>
   <phase id="atcdabbr_section_MessergebnisseKodiert-20210901T102402">
      <active pattern="template-1.2.40.0.34.6.0.11.2.95-2021-09-01T102402"/>
   </phase>
   <phase id="atcdabbr_section_MessergebnisseKodiert-20210901T102402-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.95-2021-09-01T102402-closed"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20240522T102021">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20240522T102021-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungVitalparameterEntry-20210128T145003">
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungVitalparameterEntry-20210128T145003-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.100-2021-01-28T145003-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungEntry-20210121T081221">
      <active pattern="template-1.2.40.0.34.6.0.11.3.101-2021-01-21T081221"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungEntry-20210121T081221-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.101-2021-01-21T081221-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungsGruppeEntry-20210219T125910">
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910"/>
   </phase>
   <phase id="atcdabbr_entry_SerienmessungsGruppeEntry-20210219T125910-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.102-2021-02-19T125910-closed"/>
   </phase>
   <phase id="SerienmessungsWerteEntry-20210128T145841">
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841"/>
   </phase>
   <phase id="SerienmessungsWerteEntry-20210128T145841-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.103-2021-01-28T145841-closed"/>
   </phase>
   <phase id="SerienmessungsPeriodeEntry-20210219T125919">
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919"/>
   </phase>
   <phase id="SerienmessungsPeriodeEntry-20210219T125919-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.104-2021-02-19T125919-closed"/>
   </phase>
   <phase id="atcdabrr_entry_Comment">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterGruppeEntry-20210219T130106">
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterGruppeEntry-20210219T130106-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.23-2021-02-19T130106-closed"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterEntry-20210219T130055">
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055"/>
   </phase>
   <phase id="atcdabbr_entry_VitalparameterEntry-20210219T130055-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.24-2021-02-19T130055-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedicationStatement">
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214"/>
   </phase>
   <phase id="atcdabbr_entry_MedicationStatement-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.50-2021-02-19T125214-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20230202T155045">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20230202T155045-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MessergebnisseGruppeEntry-20210218T133433">
      <active pattern="template-1.2.40.0.34.6.0.11.3.70-2021-02-18T133433"/>
   </phase>
   <phase id="atcdabbr_entry_MessergebnisseGruppeEntry-20210218T133433-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.70-2021-02-18T133433-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MessergebnisEntry-20210218T133403">
      <active pattern="template-1.2.40.0.34.6.0.11.3.71-2021-02-18T133403"/>
   </phase>
   <phase id="atcdabbr_entry_MessergebnisEntry-20210218T133403-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.71-2021-02-18T133403-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedikationsEinnahmeEntry-20210219T125222">
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222"/>
   </phase>
   <phase id="atcdabbr_entry_MedikationsEinnahmeEntry-20210219T125222-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.72-2021-02-19T125222-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceInformationOrganizer-20210831T132617">
      <active pattern="template-1.2.40.0.34.6.0.11.3.73-2021-08-31T132617"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceInformationOrganizer-20210831T132617-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.73-2021-08-31T132617-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceFirmwareRevisionInformationObservation-20210218T132434">
      <active pattern="template-1.2.40.0.34.6.0.11.3.74-2021-02-18T132434"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceFirmwareRevisionInformationObservation-20210218T132434-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.74-2021-02-18T132434-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceGMDNInformationObservation-20210218T132455">
      <active pattern="template-1.2.40.0.34.6.0.11.3.75-2021-02-18T132455"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceGMDNInformationObservation-20210218T132455-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.75-2021-02-18T132455-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceHardwareRevisionInformationObservation-20210218T132618">
      <active pattern="template-1.2.40.0.34.6.0.11.3.76-2021-02-18T132618"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceHardwareRevisionInformationObservation-20210218T132618-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.76-2021-02-18T132618-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceManufacturerInformationObservation-20210218T132716">
      <active pattern="template-1.2.40.0.34.6.0.11.3.77-2021-02-18T132716"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceManufacturerInformationObservation-20210218T132716-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.77-2021-02-18T132716-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceModelNumberInformationObservation-20210218T132809">
      <active pattern="template-1.2.40.0.34.6.0.11.3.78-2021-02-18T132809"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceModelNumberInformationObservation-20210218T132809-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.78-2021-02-18T132809-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DevicePartNumberInformationObservation-20210218T132845">
      <active pattern="template-1.2.40.0.34.6.0.11.3.79-2021-02-18T132845"/>
   </phase>
   <phase id="atcdabbr_entry_DevicePartNumberInformationObservation-20210218T132845-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.79-2021-02-18T132845-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSoftwareRevisonInformationObservation-20210218T133255">
      <active pattern="template-1.2.40.0.34.6.0.11.3.80-2021-02-18T133255"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSoftwareRevisonInformationObservation-20210218T133255-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.80-2021-02-18T133255-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation-20210218T133315">
      <active pattern="template-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation-20210218T133315-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.81-2021-02-18T133315-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSerialNumberInformationObservation-20210218T133230">
      <active pattern="template-1.2.40.0.34.6.0.11.3.82-2021-02-18T133230"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSerialNumberInformationObservation-20210218T133230-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.82-2021-02-18T133230-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceRegulationStatusInformationObservation-20210218T133016">
      <active pattern="template-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceRegulationStatusInformationObservation-20210218T133016-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.83-2021-02-18T133016-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DevicePCHAVersionInformationObservation-20210218T132912">
      <active pattern="template-1.2.40.0.34.6.0.11.3.84-2021-02-18T132912"/>
   </phase>
   <phase id="atcdabbr_entry_DevicePCHAVersionInformationObservation-20210218T132912-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.84-2021-02-18T132912-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceTimeSynchronizationInformationObservation-20210218T133337">
      <active pattern="template-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceTimeSynchronizationInformationObservation-20210218T133337-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.85-2021-02-18T133337-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceProtocolInformationObservation-20210218T132954">
      <active pattern="template-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceProtocolInformationObservation-20210218T132954-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.86-2021-02-18T132954-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceAccuracyObservation-20210218T132404">
      <active pattern="template-1.2.40.0.34.6.0.11.3.87-2021-02-18T132404"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceAccuracyObservation-20210218T132404-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.87-2021-02-18T132404-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSamplingFrequencyObservation-20210218T133208">
      <active pattern="template-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceSamplingFrequencyObservation-20210218T133208-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.88-2021-02-18T133208-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceResolutionObservation-20210218T133037">
      <active pattern="template-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceResolutionObservation-20210218T133037-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.89-2021-02-18T133037-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceMeasurementRangeObservation-20210218T132741">
      <active pattern="template-1.2.40.0.34.6.0.11.3.90-2021-02-18T132741"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceMeasurementRangeObservation-20210218T132741-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.90-2021-02-18T132741-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceProductInstanceTemplate-20220824T144823">
      <active pattern="template-1.2.40.0.34.6.0.11.9.43-2022-08-24T144823"/>
   </phase>
   <phase id="atcdabbr_entry_DeviceProductInstanceTemplate-20220824T144823-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.43-2022-08-24T144823-closed"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- MedikationVerordnungEntryemed -->
   <include href="include/1.2.40.0.34.11.8.1.3.1-2014-09-01T000000.sch"/>
   <!-- MedikationAbgabeEntryemed -->
   <include href="include/1.2.40.0.34.11.8.2.3.1-2014-09-10T000000.sch"/>
   <!-- atcdabrr_section_VitalparameterKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.46-2021-02-19T120303.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.46-2021-02-19T120303-closed.sch"/>
   <!-- atcdabbr_section_MedikationslistePSKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.63-2021-02-19T115029.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.63-2021-02-19T115029-closed.sch"/>
   <!-- atcdabbr_section_Brieftext -->
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed.sch"/>
   <!-- atcdabbr_section_Beilagen -->
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed.sch"/>
   <!-- atcdabbr_section_BehandlungsgrundUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.79-2021-02-18T131354.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.79-2021-02-18T131354-closed.sch"/>
   <!-- atcdabbr_section_Uebersetzung -->
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed.sch"/>
   <!-- atcdabbr_section_ZusammenfassungBehandlungUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.80-2021-02-18T131654.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.80-2021-02-18T131654-closed.sch"/>
   <!-- atcdabbr_section_ErhobeneDatenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.81-2021-02-18T131527.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.81-2021-02-18T131527-closed.sch"/>
   <!-- atcdabbr_section_VerwendeteGeraeteKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.82-2021-02-18T131630.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.82-2021-02-18T131630-closed.sch"/>
   <!-- atcdabbr_section_AuszuegeAusErhobeneDatenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.91-2021-02-18T131229.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.91-2021-02-18T131229-closed.sch"/>
   <!-- BodySubSectionUnkodiertGenerisch -->
   <include href="include/1.2.40.0.34.6.0.11.2.92-2021-02-19T114404.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.92-2021-02-19T114404-closed.sch"/>
   <!-- atcdabbr_section_DokumentierteEinnahmeKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.94-2021-02-19T114416.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.94-2021-02-19T114416-closed.sch"/>
   <!-- atcdabbr_section_MessergebnisseKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.95-2021-09-01T102402.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.95-2021-09-01T102402-closed.sch"/>
   <!-- atcdabbr_section_DiagnoseKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.96-2024-05-22T102021.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.96-2024-05-22T102021-closed.sch"/>
   <!-- atcdabbr_entry_SerienmessungVitalparameterEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.100-2021-01-28T145003.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.100-2021-01-28T145003-closed.sch"/>
   <!-- atcdabbr_entry_SerienmessungEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.101-2021-01-21T081221.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.101-2021-01-21T081221-closed.sch"/>
   <!-- atcdabbr_entry_SerienmessungsGruppeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.102-2021-02-19T125910.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.102-2021-02-19T125910-closed.sch"/>
   <!-- SerienmessungsWerteEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.103-2021-01-28T145841.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.103-2021-01-28T145841-closed.sch"/>
   <!-- SerienmessungsPeriodeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.104-2021-02-19T125919.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.104-2021-02-19T125919-closed.sch"/>
   <!-- atcdabrr_entry_Comment -->
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed.sch"/>
   <!-- atcdabbr_entry_externalDocument -->
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed.sch"/>
   <!-- atcdabbr_entry_EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed.sch"/>
   <!-- atcdabbr_entry_VitalparameterGruppeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.23-2021-02-19T130106.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.23-2021-02-19T130106-closed.sch"/>
   <!-- atcdabbr_entry_VitalparameterEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.24-2021-02-19T130055.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.24-2021-02-19T130055-closed.sch"/>
   <!-- atcdabbr_entry_CriticalityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed.sch"/>
   <!-- atcdabbr_entry_CertaintyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed.sch"/>
   <!-- atcdabbr_entry_SeverityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed.sch"/>
   <!-- atcdabbr_entry_ProblemStatusObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed.sch"/>
   <!-- atcdabbr_entry_MedicationStatement -->
   <include href="include/1.2.40.0.34.6.0.11.3.50-2021-02-19T125214.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.50-2021-02-19T125214-closed.sch"/>
   <!-- atcdabbr_entry_Logo -->
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed.sch"/>
   <!-- atcdabbr_entry_Problem -->
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed.sch"/>
   <!-- atcdabbr_entry_ProblemConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed.sch"/>
   <!-- atcdabbr_entry_MessergebnisseGruppeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.70-2021-02-18T133433.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.70-2021-02-18T133433-closed.sch"/>
   <!-- atcdabbr_entry_MessergebnisEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.71-2021-02-18T133403.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.71-2021-02-18T133403-closed.sch"/>
   <!-- atcdabbr_entry_MedikationsEinnahmeEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.72-2021-02-19T125222.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.72-2021-02-19T125222-closed.sch"/>
   <!-- atcdabbr_entry_DeviceInformationOrganizer -->
   <include href="include/1.2.40.0.34.6.0.11.3.73-2021-08-31T132617.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.73-2021-08-31T132617-closed.sch"/>
   <!-- atcdabbr_entry_DeviceFirmwareRevisionInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.74-2021-02-18T132434.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.74-2021-02-18T132434-closed.sch"/>
   <!-- atcdabbr_entry_DeviceGMDNInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.75-2021-02-18T132455.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.75-2021-02-18T132455-closed.sch"/>
   <!-- atcdabbr_entry_DeviceHardwareRevisionInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.76-2021-02-18T132618.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.76-2021-02-18T132618-closed.sch"/>
   <!-- atcdabbr_entry_DeviceManufacturerInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.77-2021-02-18T132716.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.77-2021-02-18T132716-closed.sch"/>
   <!-- atcdabbr_entry_DeviceModelNumberInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.78-2021-02-18T132809.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.78-2021-02-18T132809-closed.sch"/>
   <!-- atcdabbr_entry_DevicePartNumberInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.79-2021-02-18T132845.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.79-2021-02-18T132845-closed.sch"/>
   <!-- atcdabbr_entry_DeviceSoftwareRevisonInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.80-2021-02-18T133255.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.80-2021-02-18T133255-closed.sch"/>
   <!-- atcdabbr_entry_DeviceSpecificationUnspecifiedInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.81-2021-02-18T133315.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.81-2021-02-18T133315-closed.sch"/>
   <!-- atcdabbr_entry_DeviceSerialNumberInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.82-2021-02-18T133230.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.82-2021-02-18T133230-closed.sch"/>
   <!-- atcdabbr_entry_DeviceRegulationStatusInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.83-2021-02-18T133016.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.83-2021-02-18T133016-closed.sch"/>
   <!-- atcdabbr_entry_DevicePCHAVersionInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.84-2021-02-18T132912.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.84-2021-02-18T132912-closed.sch"/>
   <!-- atcdabbr_entry_DeviceTimeSynchronizationInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.85-2021-02-18T133337.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.85-2021-02-18T133337-closed.sch"/>
   <!-- atcdabbr_entry_DeviceProtocolInformationObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.86-2021-02-18T132954.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.86-2021-02-18T132954-closed.sch"/>
   <!-- atcdabbr_entry_DeviceAccuracyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.87-2021-02-18T132404.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.87-2021-02-18T132404-closed.sch"/>
   <!-- atcdabbr_entry_DeviceSamplingFrequencyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.88-2021-02-18T133208.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.88-2021-02-18T133208-closed.sch"/>
   <!-- atcdabbr_entry_DeviceResolutionObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.89-2021-02-18T133037.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.89-2021-02-18T133037-closed.sch"/>
   <!-- atcdabbr_entry_DeviceMeasurementRangeObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.90-2021-02-18T132741.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.90-2021-02-18T132741-closed.sch"/>
   <!-- atcdabbr_other_PerformerBody -->
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed.sch"/>
   <!-- atcdabbr_entry_DeviceProductInstanceTemplate -->
   <include href="include/1.2.40.0.34.6.0.11.9.43-2022-08-24T144823.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.43-2022-08-24T144823-closed.sch"/>

</schema>
<!-- versionLabel="1.6.0+20240709-1.2.1" -->
