<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Befund bildgebende Diagnostik v3 (1.2.40.0.34.777.16.4.2 2023-08-29T14:31:57)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:hl7-org:sdtc" prefix="sdtc"/>
   <ns uri="urn:hl7-at:v3" prefix="hl7at"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <ns uri="urn:hl7-org:ips" prefix="ips"/>
   <!-- Include realm specific schematron -->
   <!-- Include scenario label -->
   <let name="scenariolabel" value="'BefundBildgebendeDiagnostik'"/>
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario BefundBildgebendeDiagnostik -->

   <!-- atbgd_document_BefundBildgebendeDiagnostik -->
   <pattern>
      <title>atbgd_document_BefundBildgebendeDiagnostik</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.5.9.3'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.17']]"
                 see="https://elga.art-decor.org/elgabgd-html-20250116T125424/tmp-1.2.40.0.34.6.0.11.0.17-2024-11-07T130921.html">(atbgd_document_BefundBildgebendeDiagnostik): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.1'] and hl7:templateId[@root = '1.2.40.0.34.7.5.9.3'] and hl7:templateId[@root = '1.2.40.0.34.6.0.11.0.17']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.6.0.11.0.17-2024-11-07T130921.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.0.17-2024-11-07T130921-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.17-2024-11-07T130921"/>
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
      <active pattern="template-1.2.40.0.34.11.13.3.13-2017-07-13T210449"/>
      <active pattern="template-1.2.40.0.34.11.13.3.18-2017-08-10T195216"/>
      <active pattern="template-1.2.40.0.34.11.13.3.19-2017-08-10T202554"/>
      <active pattern="template-1.2.40.0.34.11.13.3.21-2017-08-20T120835"/>
      <active pattern="template-1.2.40.0.34.11.13.3.6-2017-01-26T142954"/>
      <active pattern="template-1.2.40.0.34.11.13.3.7-2017-08-13T150852"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.131-2024-02-27T135040"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.132-2024-02-27T142310"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.133-2024-02-27T142632"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.134-2024-02-27T143217"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.135-2024-02-27T144130"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2024-05-21T145508"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2024-05-22T101451"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2024-05-22T100110"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.84-2024-02-27T101120"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021"/>
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2024-05-21T152134"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.182-2024-03-19T145537"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.183-2024-02-27T132448"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.184-2024-05-21T100055"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2024-05-23T150255"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.41-2024-05-22T082531"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2024-05-22T095300"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2024-05-22T095012"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910"/>
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446"/>
   </phase>
   <phase id="atbgd_document_BefundBildgebendeDiagnostik">
      <active pattern="template-1.2.40.0.34.6.0.11.0.17-2024-11-07T130921"/>
   </phase>
   <phase id="atbgd_document_BefundBildgebendeDiagnostik-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.0.17-2024-11-07T130921-closed"/>
   </phase>
   <phase id="EingebettetesObjektEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
   </phase>
   <phase id="ProblemStatusObservationEntry-20170713T210449">
      <active pattern="template-1.2.40.0.34.11.13.3.13-2017-07-13T210449"/>
   </phase>
   <phase id="CriticalityObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.18-2017-08-10T195216"/>
   </phase>
   <phase id="CertaintyObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.19-2017-08-10T202554"/>
   </phase>
   <phase id="SeverityObservation">
      <active pattern="template-1.2.40.0.34.11.13.3.21-2017-08-20T120835"/>
   </phase>
   <phase id="GesundheitsproblemBedenkenEntry">
      <active pattern="template-1.2.40.0.34.11.13.3.6-2017-01-26T142954"/>
   </phase>
   <phase id="ProblemEntryGesundheitsproblem-20170813T150852">
      <active pattern="template-1.2.40.0.34.11.13.3.7-2017-08-13T150852"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643"/>
   </phase>
   <phase id="atcdabrr_section_ImpfungenKodiert-20210219T114643-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed"/>
   </phase>
   <phase id="elgagab_section_anamnese-20210219T084636">
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636"/>
   </phase>
   <phase id="elgagab_section_anamnese-20210219T084636-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.10-2021-02-19T084636-closed"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeUnkodiert-20210219T104146">
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146"/>
   </phase>
   <phase id="elgagab_section_diagnostikUndBefundeUnkodiert-20210219T104146-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.11-2021-02-19T104146-closed"/>
   </phase>
   <phase id="elgabgd_section_Anforderung">
      <active pattern="template-1.2.40.0.34.6.0.11.2.131-2024-02-27T135040"/>
   </phase>
   <phase id="elgabgd_section_Anforderung-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.131-2024-02-27T135040-closed"/>
   </phase>
   <phase id="elgabgd_section_FruehereUntersuchungenUndBefunde">
      <active pattern="template-1.2.40.0.34.6.0.11.2.132-2024-02-27T142310"/>
   </phase>
   <phase id="elgabgd_section_FruehereUntersuchungenUndBefunde-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.132-2024-02-27T142310-closed"/>
   </phase>
   <phase id="elgabgd_section_Komplikationen">
      <active pattern="template-1.2.40.0.34.6.0.11.2.133-2024-02-27T142632"/>
   </phase>
   <phase id="elgabgd_section_Komplikationen-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.133-2024-02-27T142632-closed"/>
   </phase>
   <phase id="elgabgd_section_Befund">
      <active pattern="template-1.2.40.0.34.6.0.11.2.134-2024-02-27T143217"/>
   </phase>
   <phase id="elgabgd_section_Befund-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.134-2024-02-27T143217-closed"/>
   </phase>
   <phase id="elgabgd_section_Verdachtsdiagnose">
      <active pattern="template-1.2.40.0.34.6.0.11.2.135-2024-02-27T144130"/>
   </phase>
   <phase id="elgabgd_section_Verdachtsdiagnose-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.135-2024-02-27T144130-closed"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenUnkodiert-20240521T145508">
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2024-05-21T145508"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenUnkodiert-20240521T145508-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.16-2024-05-21T145508-closed"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundUnkodiert-20210219T092953">
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953"/>
   </phase>
   <phase id="elgagab_section_KonsultUeberweisungsgrundUnkodiert-20210219T092953-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.17-2021-02-19T092953-closed"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeDiagnostik-20240522T101451">
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2024-05-22T101451"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeDiagnostik-20240522T101451-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.19-2024-05-22T101451-closed"/>
   </phase>
   <phase id="elgagab_section_Status-20240522T100110">
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2024-05-22T100110"/>
   </phase>
   <phase id="elgagab_section_Status-20240522T100110-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.20-2024-05-22T100110-closed"/>
   </phase>
   <phase id="elgagab_section_weitereEmpfohleneMassnahmenUnkodiert-20210219T104339">
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339"/>
   </phase>
   <phase id="elgagab_section_weitereEmpfohleneMassnahmenUnkodiert-20210219T104339-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.23-2021-02-19T104339-closed"/>
   </phase>
   <phase id="elgagab_section_ZusammenfassendeBeurteilung-20210219T104512">
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512"/>
   </phase>
   <phase id="elgagab_section_ZusammenfassendeBeurteilung-20210219T104512-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.25-2021-02-19T104512-closed"/>
   </phase>
   <phase id="elgagab_section_TermineKontrollenWiederbestellungen-20210219T104151">
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151"/>
   </phase>
   <phase id="elgagab_section_TermineKontrollenWiederbestellungen-20210219T104151-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.42-2021-02-19T104151-closed"/>
   </phase>
   <phase id="elgagab_section_geplanteUntersuchungen-20210219T090640">
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640"/>
   </phase>
   <phase id="elgagab_section_geplanteUntersuchungen-20210219T090640-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.43-2021-02-19T090640-closed"/>
   </phase>
   <phase id="elgagab_section_konservativeTherapie-20210219T093018">
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018"/>
   </phase>
   <phase id="elgagab_section_konservativeTherapie-20210219T093018-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.44-2021-02-19T093018-closed"/>
   </phase>
   <phase id="elgagab_section_chirurgischeTherapie-20210219T090340">
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340"/>
   </phase>
   <phase id="elgagab_section_chirurgischeTherapie-20210219T090340-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.45-2021-02-19T090340-closed"/>
   </phase>
   <phase id="atcdabrr_section_SchwangerschaftenKodiert-20210128T144037">
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037"/>
   </phase>
   <phase id="atcdabrr_section_SchwangerschaftenKodiert-20210128T144037-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.48-2021-01-28T144037-closed"/>
   </phase>
   <phase id="elgagab_section_SchwangerschaftenUnkodiert-20210219T103539">
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539"/>
   </phase>
   <phase id="elgagab_section_SchwangerschaftenUnkodiert-20210219T103539-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.49-2021-02-19T103539-closed"/>
   </phase>
   <phase id="elgagab_section_ImplantateUnkodiert-20210219T103530">
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530"/>
   </phase>
   <phase id="elgagab_section_ImplantateUnkodiert-20210219T103530-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.50-2021-02-19T103530-closed"/>
   </phase>
   <phase id="cdagab_section_BeeintraechtigungenKodiert-20240521T153717">
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717"/>
   </phase>
   <phase id="cdagab_section_BeeintraechtigungenKodiert-20240521T153717-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.51-2024-05-21T153717-closed"/>
   </phase>
   <phase id="elgagab_section_BeeintraechtigungenUnkodiert-20210219T090348">
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348"/>
   </phase>
   <phase id="elgagab_section_BeeintraechtigungenUnkodiert-20210219T090348-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.52-2021-02-19T090348-closed"/>
   </phase>
   <phase id="elgagab_section_ImpfungenUnkodiert-20210219T093025">
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025"/>
   </phase>
   <phase id="elgagab_section_ImpfungenUnkodiert-20210219T093025-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.53-2021-02-19T093025-closed"/>
   </phase>
   <phase id="elgagab_section_LebensstilUnkodiert-20210219T092815">
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815"/>
   </phase>
   <phase id="elgagab_section_LebensstilUnkodiert-20210219T092815-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.54-2021-02-19T092815-closed"/>
   </phase>
   <phase id="elgagab_section_LebensstilKodiert-20210219T092947">
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947"/>
   </phase>
   <phase id="elgagab_section_LebensstilKodiert-20210219T092947-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.55-2021-02-19T092947-closed"/>
   </phase>
   <phase id="atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert-20210219T115750">
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750"/>
   </phase>
   <phase id="atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert-20210219T115750-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.60-2021-02-19T115750-closed"/>
   </phase>
   <phase id="elgagab_section_VitalparameterUnkodiert-20210219T104313">
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313"/>
   </phase>
   <phase id="elgagab_section_VitalparameterUnkodiert-20210219T104313-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.68-2021-02-19T104313-closed"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935"/>
   </phase>
   <phase id="atcdabbr_section_Brieftext-20210628T111935-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503"/>
   </phase>
   <phase id="atcdabbr_section_AbschliessendeBemerkung-20210628T112503-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058"/>
   </phase>
   <phase id="atcdabbr_section_Beilagen-20230405T134058-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed"/>
   </phase>
   <phase id="atcdabrr_section_AusstehendeBefunde-20210219T114054">
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054"/>
   </phase>
   <phase id="atcdabrr_section_AusstehendeBefunde-20210219T114054-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.72-2021-02-19T114054-closed"/>
   </phase>
   <phase id="atcdabbr_section_EmpfohleneAnordnungenPflege-20210219T114423">
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423"/>
   </phase>
   <phase id="atcdabbr_section_EmpfohleneAnordnungenPflege-20210219T114423-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.73-2021-02-19T114423-closed"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert-20210219T090717">
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717"/>
   </phase>
   <phase id="elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert-20210219T090717-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.74-2021-02-19T090717-closed"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152"/>
   </phase>
   <phase id="atcdabbr_section_Uebersetzung-20230413T110152-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed"/>
   </phase>
   <phase id="elgabgd_section_DurchgefuehrteUntersuchung">
      <active pattern="template-1.2.40.0.34.6.0.11.2.84-2024-02-27T101120"/>
   </phase>
   <phase id="elgabgd_section_DurchgefuehrteUntersuchung-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.84-2024-02-27T101120-closed"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20240522T102021">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021"/>
   </phase>
   <phase id="atcdabbr_section_DiagnoseKodiert-20240522T102021-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.96-2024-05-22T102021-closed"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeAnamnese-20240521T152134">
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2024-05-21T152134"/>
   </phase>
   <phase id="elgagab_section_FachspezifischeAnamnese-20240521T152134-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.2.98-2024-05-21T152134-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20241105T143545">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545"/>
   </phase>
   <phase id="atcdabbr_entry_Immunization-20241105T143545-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.1-2024-11-05T143545-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20250107T115116">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationSchedule-20250107T115116-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.10-2025-01-07T115116-closed"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256"/>
   </phase>
   <phase id="atcdabrr_entry_Comment-20210219T124256-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204"/>
   </phase>
   <phase id="atcdabbr_entry_externalDocument-20230413T110204-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20230509T163856">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856"/>
   </phase>
   <phase id="atcdabbr_entry_Zusatzklassifikation-20230509T163856-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.168-2023-05-09T163856-closed"/>
   </phase>
   <phase id="elgabgd_entry_KlassifikationEntry">
      <active pattern="template-1.2.40.0.34.6.0.11.3.182-2024-03-19T145537"/>
   </phase>
   <phase id="elgabgd_entry_KlassifikationEntry-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.182-2024-03-19T145537-closed"/>
   </phase>
   <phase id="elgabgd_entry_StrahlenexpositionEntry">
      <active pattern="template-1.2.40.0.34.6.0.11.3.183-2024-02-27T132448"/>
   </phase>
   <phase id="elgabgd_entry_StrahlenexpositionEntry-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.183-2024-02-27T132448-closed"/>
   </phase>
   <phase id="elgabgd_entry_RadiopharmakonEntry">
      <active pattern="template-1.2.40.0.34.6.0.11.3.184-2024-05-21T100055"/>
   </phase>
   <phase id="elgabgd_entry_RadiopharmakonEntry-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.184-2024-05-21T100055-closed"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236"/>
   </phase>
   <phase id="atcdabbr_entry_EingebettetesObjektEntry-20230509T164236-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20230606T145012">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationTarget-20230606T145012-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.2-2023-06-06T145012-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20241115T083802">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationImpfungNichtAngegeben-20241115T083802-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.28-2024-11-15T083802-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402"/>
   </phase>
   <phase id="atcdabbr_entry_CriticalityObservation-20210628T134402-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249"/>
   </phase>
   <phase id="atcdabbr_entry_CertaintyObservation-20210219T124249-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038"/>
   </phase>
   <phase id="atcdabbr_entry_SeverityObservation-20210219T130038-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed"/>
   </phase>
   <phase id="atcdabbr_entry_MedicalDevice-20210219T125204">
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204"/>
   </phase>
   <phase id="atcdabbr_entry_MedicalDevice-20210219T125204-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.39-2021-02-19T125204-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumQuantitativ-20240523T150255">
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2024-05-23T150255"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumQuantitativ-20240523T150255-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.40-2024-05-23T150255-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilTabakkonsumNominal-20240522T082531">
      <active pattern="template-1.2.40.0.34.6.0.11.3.41-2024-05-22T082531"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ-20240522T095300">
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2024-05-22T095300"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ-20240522T095300-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.42-2024-05-22T095300-closed"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumNominal-20240522T095012">
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2024-05-22T095012"/>
   </phase>
   <phase id="atcdabbr_entry_LebensstilAlkoholkonsumNominal-20240522T095012-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.43-2024-05-22T095012-closed"/>
   </phase>
   <phase id="atcdabbr_entry_AktuelleSchwangerschaft-20240521T153303">
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303"/>
   </phase>
   <phase id="atcdabbr_entry_AktuelleSchwangerschaft-20240521T153303-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.44-2024-05-21T153303-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ErwartetesGeburtsdatum-20210201T140251">
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251"/>
   </phase>
   <phase id="atcdabbr_entry_ErwartetesGeburtsdatum-20210201T140251-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.45-2021-02-01T140251-closed"/>
   </phase>
   <phase id="atcdabbr_entry_BisherigeSchwangerschaften-20210127T110027">
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027"/>
   </phase>
   <phase id="atcdabbr_entry_BisherigeSchwangerschaften-20210127T110027-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.46-2021-01-27T110027-closed"/>
   </phase>
   <phase id="atcdabbr_entry_FunctionalStatus-20210219T124346">
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346"/>
   </phase>
   <phase id="atcdabbr_entry_FunctionalStatus-20210219T124346-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.47-2021-02-19T124346-closed"/>
   </phase>
   <phase id="ELGA_HistoryOfProcedures-20230203T102536">
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536"/>
   </phase>
   <phase id="ELGA_HistoryOfProcedures-20230203T102536-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.48-2023-02-03T102536-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemStatusObservation-20210219T125553-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20230407T103811">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811"/>
   </phase>
   <phase id="atcdabbr_entry_ImmunizationBillability-20230407T103811-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.5-2023-04-07T103811-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849"/>
   </phase>
   <phase id="atcdabbr_entry_Logo-20210628T110849-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20230202T155045">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045"/>
   </phase>
   <phase id="atcdabbr_entry_Problem-20230202T155045-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533"/>
   </phase>
   <phase id="atcdabbr_entry_ProblemConcern-20210219T125533-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyTranscriber-20210804T154113-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBody-20210219T133615-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337"/>
   </phase>
   <phase id="atcdabbr_other_PerformerBodyImpfendePerson-20211013T125337-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProductNichtAngegeben-20210219T133513-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20230405T101745">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745"/>
   </phase>
   <phase id="atcdabbr_other_vaccineProduct-20230405T101745-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.32-2023-04-05T101745-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier-20230407T105453">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyVerifier-20230407T105453-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.44-2023-04-07T105453-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor-20230404T103910">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyAuthorizedEditor-20230404T103910-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.46-2023-04-04T103910-closed"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446"/>
   </phase>
   <phase id="atcdabbr_other_ParticipantBodyDataEnterer-closed">
      <active pattern="template-1.2.40.0.34.6.0.11.9.47-2023-02-28T124446-closed"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.11.1.3.1-2017-05-05T000000.sch"/>
   <!-- ProblemStatusObservationEntry -->
   <include href="include/1.2.40.0.34.11.13.3.13-2017-07-13T210449.sch"/>
   <!-- CriticalityObservation -->
   <include href="include/1.2.40.0.34.11.13.3.18-2017-08-10T195216.sch"/>
   <!-- CertaintyObservation -->
   <include href="include/1.2.40.0.34.11.13.3.19-2017-08-10T202554.sch"/>
   <!-- SeverityObservation -->
   <include href="include/1.2.40.0.34.11.13.3.21-2017-08-20T120835.sch"/>
   <!-- GesundheitsproblemBedenkenEntry -->
   <include href="include/1.2.40.0.34.11.13.3.6-2017-01-26T142954.sch"/>
   <!-- ProblemEntryGesundheitsproblem -->
   <include href="include/1.2.40.0.34.11.13.3.7-2017-08-13T150852.sch"/>
   <!-- atcdabrr_section_ImpfungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.1-2021-02-19T114643-closed.sch"/>
   <!-- elgagab_section_anamnese -->
   <include href="include/1.2.40.0.34.6.0.11.2.10-2021-02-19T084636.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.10-2021-02-19T084636-closed.sch"/>
   <!-- elgagab_section_diagnostikUndBefundeUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.11-2021-02-19T104146.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.11-2021-02-19T104146-closed.sch"/>
   <!-- elgabgd_section_Anforderung -->
   <include href="include/1.2.40.0.34.6.0.11.2.131-2024-02-27T135040.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.131-2024-02-27T135040-closed.sch"/>
   <!-- elgabgd_section_FruehereUntersuchungenUndBefunde -->
   <include href="include/1.2.40.0.34.6.0.11.2.132-2024-02-27T142310.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.132-2024-02-27T142310-closed.sch"/>
   <!-- elgabgd_section_Komplikationen -->
   <include href="include/1.2.40.0.34.6.0.11.2.133-2024-02-27T142632.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.133-2024-02-27T142632-closed.sch"/>
   <!-- elgabgd_section_Befund -->
   <include href="include/1.2.40.0.34.6.0.11.2.134-2024-02-27T143217.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.134-2024-02-27T143217-closed.sch"/>
   <!-- elgabgd_section_Verdachtsdiagnose -->
   <include href="include/1.2.40.0.34.6.0.11.2.135-2024-02-27T144130.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.135-2024-02-27T144130-closed.sch"/>
   <!-- elgagab_section_FruehereErkrankungenMassnahmenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.16-2024-05-21T145508.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.16-2024-05-21T145508-closed.sch"/>
   <!-- elgagab_section_KonsultUeberweisungsgrundUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.17-2021-02-19T092953.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.17-2021-02-19T092953-closed.sch"/>
   <!-- elgagab_section_FachspezifischeDiagnostik -->
   <include href="include/1.2.40.0.34.6.0.11.2.19-2024-05-22T101451.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.19-2024-05-22T101451-closed.sch"/>
   <!-- elgagab_section_Status -->
   <include href="include/1.2.40.0.34.6.0.11.2.20-2024-05-22T100110.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.20-2024-05-22T100110-closed.sch"/>
   <!-- elgagab_section_weitereEmpfohleneMassnahmenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.23-2021-02-19T104339.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.23-2021-02-19T104339-closed.sch"/>
   <!-- elgagab_section_ZusammenfassendeBeurteilung -->
   <include href="include/1.2.40.0.34.6.0.11.2.25-2021-02-19T104512.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.25-2021-02-19T104512-closed.sch"/>
   <!-- elgagab_section_TermineKontrollenWiederbestellungen -->
   <include href="include/1.2.40.0.34.6.0.11.2.42-2021-02-19T104151.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.42-2021-02-19T104151-closed.sch"/>
   <!-- elgagab_section_geplanteUntersuchungen -->
   <include href="include/1.2.40.0.34.6.0.11.2.43-2021-02-19T090640.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.43-2021-02-19T090640-closed.sch"/>
   <!-- elgagab_section_konservativeTherapie -->
   <include href="include/1.2.40.0.34.6.0.11.2.44-2021-02-19T093018.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.44-2021-02-19T093018-closed.sch"/>
   <!-- elgagab_section_chirurgischeTherapie -->
   <include href="include/1.2.40.0.34.6.0.11.2.45-2021-02-19T090340.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.45-2021-02-19T090340-closed.sch"/>
   <!-- atcdabrr_section_SchwangerschaftenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.48-2021-01-28T144037.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.48-2021-01-28T144037-closed.sch"/>
   <!-- elgagab_section_SchwangerschaftenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.49-2021-02-19T103539.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.49-2021-02-19T103539-closed.sch"/>
   <!-- elgagab_section_ImplantateUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.50-2021-02-19T103530.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.50-2021-02-19T103530-closed.sch"/>
   <!-- cdagab_section_BeeintraechtigungenKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.51-2024-05-21T153717.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.51-2024-05-21T153717-closed.sch"/>
   <!-- elgagab_section_BeeintraechtigungenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.52-2021-02-19T090348.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.52-2021-02-19T090348-closed.sch"/>
   <!-- elgagab_section_ImpfungenUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.53-2021-02-19T093025.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.53-2021-02-19T093025-closed.sch"/>
   <!-- elgagab_section_LebensstilUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.54-2021-02-19T092815.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.54-2021-02-19T092815-closed.sch"/>
   <!-- elgagab_section_LebensstilKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.55-2021-02-19T092947.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.55-2021-02-19T092947-closed.sch"/>
   <!-- atcdabrr_section_MedizinischeGeraeteUndImplantateKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.60-2021-02-19T115750.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.60-2021-02-19T115750-closed.sch"/>
   <!-- elgagab_section_VitalparameterUnkodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.68-2021-02-19T104313.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.68-2021-02-19T104313-closed.sch"/>
   <!-- atcdabbr_section_Brieftext -->
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.69-2021-06-28T111935-closed.sch"/>
   <!-- atcdabbr_section_AbschliessendeBemerkung -->
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.70-2021-06-28T112503-closed.sch"/>
   <!-- atcdabbr_section_Beilagen -->
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.71-2023-04-05T134058-closed.sch"/>
   <!-- atcdabrr_section_AusstehendeBefunde -->
   <include href="include/1.2.40.0.34.6.0.11.2.72-2021-02-19T114054.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.72-2021-02-19T114054-closed.sch"/>
   <!-- atcdabbr_section_EmpfohleneAnordnungenPflege -->
   <include href="include/1.2.40.0.34.6.0.11.2.73-2021-02-19T114423.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.73-2021-02-19T114423-closed.sch"/>
   <!-- elgagab_section_FruehereErkrankungenMassnahmenICD10Kodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.74-2021-02-19T090717.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.74-2021-02-19T090717-closed.sch"/>
   <!-- atcdabbr_section_Uebersetzung -->
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.8-2023-04-13T110152-closed.sch"/>
   <!-- elgabgd_section_DurchgefuehrteUntersuchung -->
   <include href="include/1.2.40.0.34.6.0.11.2.84-2024-02-27T101120.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.84-2024-02-27T101120-closed.sch"/>
   <!-- atcdabbr_section_DiagnoseKodiert -->
   <include href="include/1.2.40.0.34.6.0.11.2.96-2024-05-22T102021.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.96-2024-05-22T102021-closed.sch"/>
   <!-- elgagab_section_FachspezifischeAnamnese -->
   <include href="include/1.2.40.0.34.6.0.11.2.98-2024-05-21T152134.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.2.98-2024-05-21T152134-closed.sch"/>
   <!-- atcdabbr_entry_Immunization -->
   <include href="include/1.2.40.0.34.6.0.11.3.1-2024-11-05T143545.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.1-2024-11-05T143545-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationSchedule -->
   <include href="include/1.2.40.0.34.6.0.11.3.10-2025-01-07T115116.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.10-2025-01-07T115116-closed.sch"/>
   <!-- atcdabrr_entry_Comment -->
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.11-2021-02-19T124256-closed.sch"/>
   <!-- atcdabbr_entry_externalDocument -->
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.14-2023-04-13T110204-closed.sch"/>
   <!-- atcdabbr_entry_Zusatzklassifikation -->
   <include href="include/1.2.40.0.34.6.0.11.3.168-2023-05-09T163856.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.168-2023-05-09T163856-closed.sch"/>
   <!-- elgabgd_entry_KlassifikationEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.182-2024-03-19T145537.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.182-2024-03-19T145537-closed.sch"/>
   <!-- elgabgd_entry_StrahlenexpositionEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.183-2024-02-27T132448.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.183-2024-02-27T132448-closed.sch"/>
   <!-- elgabgd_entry_RadiopharmakonEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.184-2024-05-21T100055.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.184-2024-05-21T100055-closed.sch"/>
   <!-- atcdabbr_entry_EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.19-2023-05-09T164236-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationTarget -->
   <include href="include/1.2.40.0.34.6.0.11.3.2-2023-06-06T145012.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.2-2023-06-06T145012-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationImpfungNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.3.28-2024-11-15T083802.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.28-2024-11-15T083802-closed.sch"/>
   <!-- atcdabbr_entry_CriticalityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.35-2021-06-28T134402-closed.sch"/>
   <!-- atcdabbr_entry_CertaintyObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.36-2021-02-19T124249-closed.sch"/>
   <!-- atcdabbr_entry_SeverityObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.38-2021-02-19T130038-closed.sch"/>
   <!-- atcdabbr_entry_MedicalDevice -->
   <include href="include/1.2.40.0.34.6.0.11.3.39-2021-02-19T125204.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.39-2021-02-19T125204-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilTabakkonsumQuantitativ -->
   <include href="include/1.2.40.0.34.6.0.11.3.40-2024-05-23T150255.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.40-2024-05-23T150255-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilTabakkonsumNominal -->
   <include href="include/1.2.40.0.34.6.0.11.3.41-2024-05-22T082531.sch"/>
   <!-- atcdabbr_entry_LebensstilAlkoholkonsumQuantitativ -->
   <include href="include/1.2.40.0.34.6.0.11.3.42-2024-05-22T095300.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.42-2024-05-22T095300-closed.sch"/>
   <!-- atcdabbr_entry_LebensstilAlkoholkonsumNominal -->
   <include href="include/1.2.40.0.34.6.0.11.3.43-2024-05-22T095012.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.43-2024-05-22T095012-closed.sch"/>
   <!-- atcdabbr_entry_AktuelleSchwangerschaft -->
   <include href="include/1.2.40.0.34.6.0.11.3.44-2024-05-21T153303.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.44-2024-05-21T153303-closed.sch"/>
   <!-- atcdabbr_entry_ErwartetesGeburtsdatum -->
   <include href="include/1.2.40.0.34.6.0.11.3.45-2021-02-01T140251.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.45-2021-02-01T140251-closed.sch"/>
   <!-- atcdabbr_entry_BisherigeSchwangerschaften -->
   <include href="include/1.2.40.0.34.6.0.11.3.46-2021-01-27T110027.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.46-2021-01-27T110027-closed.sch"/>
   <!-- atcdabbr_entry_FunctionalStatus -->
   <include href="include/1.2.40.0.34.6.0.11.3.47-2021-02-19T124346.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.47-2021-02-19T124346-closed.sch"/>
   <!-- ELGA_HistoryOfProcedures -->
   <include href="include/1.2.40.0.34.6.0.11.3.48-2023-02-03T102536.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.48-2023-02-03T102536-closed.sch"/>
   <!-- atcdabbr_entry_ProblemStatusObservation -->
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.49-2021-02-19T125553-closed.sch"/>
   <!-- atcdabbr_entry_ImmunizationBillability -->
   <include href="include/1.2.40.0.34.6.0.11.3.5-2023-04-07T103811.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.5-2023-04-07T103811-closed.sch"/>
   <!-- atcdabbr_entry_Logo -->
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.53-2021-06-28T110849-closed.sch"/>
   <!-- atcdabbr_entry_Problem -->
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.6-2023-02-02T155045-closed.sch"/>
   <!-- atcdabbr_entry_ProblemConcern -->
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.3.7-2021-02-19T125533-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyTranscriber -->
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.14-2021-08-04T154113-closed.sch"/>
   <!-- atcdabbr_other_PerformerBody -->
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.17-2021-02-19T133615-closed.sch"/>
   <!-- atcdabbr_other_PerformerBodyImpfendePerson -->
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.21-2021-10-13T125337-closed.sch"/>
   <!-- atcdabbr_other_vaccineProductNichtAngegeben -->
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.31-2021-02-19T133513-closed.sch"/>
   <!-- atcdabbr_other_vaccineProduct -->
   <include href="include/1.2.40.0.34.6.0.11.9.32-2023-04-05T101745.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.32-2023-04-05T101745-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyVerifier -->
   <include href="include/1.2.40.0.34.6.0.11.9.44-2023-04-07T105453.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.44-2023-04-07T105453-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyAuthorizedEditor -->
   <include href="include/1.2.40.0.34.6.0.11.9.46-2023-04-04T103910.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.46-2023-04-04T103910-closed.sch"/>
   <!-- atcdabbr_other_ParticipantBodyDataEnterer -->
   <include href="include/1.2.40.0.34.6.0.11.9.47-2023-02-28T124446.sch"/>
   <include href="include/1.2.40.0.34.6.0.11.9.47-2023-02-28T124446-closed.sch"/>

</schema>
<!-- versionLabel="1.1.0+20250116-3.0.0" -->
