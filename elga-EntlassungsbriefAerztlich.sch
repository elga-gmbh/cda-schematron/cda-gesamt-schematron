<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2">
   <title>Schematron file for transaction Entlassungsbrief Ärztlich v2 (1.2.40.0.34.77.4.102 2023-05-09T07:22:43)</title>
   <ns uri="urn:hl7-org:v3" prefix="hl7"/>
   <ns uri="urn:hl7-org:v3" prefix="cda"/>
   <ns uri="http://www.w3.org/2001/XMLSchema-instance" prefix="xsi"/>
   <ns uri="http://www.w3.org/2001/XMLSchema" prefix="xs"/>
   <!-- Add extra namespaces -->
   <ns uri="urn:ihe:pharm:medication" prefix="pharm"/>
   <ns uri="http://www.w3.org/XML/1998/namespace" prefix="xml"/>
   <!-- Include realm specific schematron -->
   <!-- Include scenario label -->
   <let name="scenariolabel" value="'EntlassungsbriefAerztlich'"/>
   <include href="include/check-CDA-tables.AT.sch"/>
   <!-- Include datatype abstract schematrons -->
   <pattern>
      <include href="include/DTr1_AD.sch"/>
      <include href="include/DTr1_AD.CA.sch"/>
      <include href="include/DTr1_AD.CA.BASIC.sch"/>
      <include href="include/DTr1_AD.DE.sch"/>
      <include href="include/DTr1_AD.EPSOS.sch"/>
      <include href="include/DTr1_AD.IPS.sch"/>
      <include href="include/DTr1_AD.NL.sch"/>
      <include href="include/DTr1_ADXP.sch"/>
      <include href="include/DTr1_ANY.sch"/>
      <include href="include/DTr1_BIN.sch"/>
      <include href="include/DTr1_BL.sch"/>
      <include href="include/DTr1_BN.sch"/>
      <include href="include/DTr1_BXIT_IVL_PQ.sch"/>
      <include href="include/DTr1_CD.sch"/>
      <include href="include/DTr1_CD.EPSOS.sch"/>
      <include href="include/DTr1_CD.IPS.sch"/>
      <include href="include/DTr1_CD.SDTC.sch"/>
      <include href="include/DTr1_CE.sch"/>
      <include href="include/DTr1_CE.EPSOS.sch"/>
      <include href="include/DTr1_CE.IPS.sch"/>
      <include href="include/DTr1_CO.sch"/>
      <include href="include/DTr1_CO.EPSOS.sch"/>
      <include href="include/DTr1_CR.sch"/>
      <include href="include/DTr1_CS.sch"/>
      <include href="include/DTr1_CS.LANG.sch"/>
      <include href="include/DTr1_CV.sch"/>
      <include href="include/DTr1_CV.EPSOS.sch"/>
      <include href="include/DTr1_CV.IPS.sch"/>
      <include href="include/DTr1_ED.sch"/>
      <include href="include/DTr1_EIVL.event.sch"/>
      <include href="include/DTr1_EIVL_TS.sch"/>
      <include href="include/DTr1_EN.sch"/>
      <include href="include/DTr1_ENXP.sch"/>
      <include href="include/DTr1_GLIST.sch"/>
      <include href="include/DTr1_GLIST_PQ.sch"/>
      <include href="include/DTr1_GLIST_TS.sch"/>
      <include href="include/DTr1_hl7nl-INT.sch"/>
      <include href="include/DTr1_hl7nl-IVL_QTY.sch"/>
      <include href="include/DTr1_hl7nl-IVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PIVL_TS.sch"/>
      <include href="include/DTr1_hl7nl-PQ.sch"/>
      <include href="include/DTr1_hl7nl-QSET_QTY.sch"/>
      <include href="include/DTr1_hl7nl-RTO.sch"/>
      <include href="include/DTr1_hl7nl-TS.sch"/>
      <include href="include/DTr1_II.sch"/>
      <include href="include/DTr1_II.AT.ATU.sch"/>
      <include href="include/DTr1_II.AT.BLZ.sch"/>
      <include href="include/DTr1_II.AT.DVR.sch"/>
      <include href="include/DTr1_II.AT.KTONR.sch"/>
      <include href="include/DTr1_II.EPSOS.sch"/>
      <include href="include/DTr1_II.NL.AGB.sch"/>
      <include href="include/DTr1_II.NL.BIG.sch"/>
      <include href="include/DTr1_II.NL.BSN.sch"/>
      <include href="include/DTr1_II.NL.URA.sch"/>
      <include href="include/DTr1_II.NL.UZI.sch"/>
      <include href="include/DTr1_INT.sch"/>
      <include href="include/DTr1_INT.NONNEG.sch"/>
      <include href="include/DTr1_INT.POS.sch"/>
      <include href="include/DTr1_IVL_INT.sch"/>
      <include href="include/DTr1_IVL_MO.sch"/>
      <include href="include/DTr1_IVL_PQ.sch"/>
      <include href="include/DTr1_IVL_REAL.sch"/>
      <include href="include/DTr1_IVL_TS.sch"/>
      <include href="include/DTr1_IVL_TS.CH.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_IVL_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_IVL_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_IVXB_INT.sch"/>
      <include href="include/DTr1_IVXB_MO.sch"/>
      <include href="include/DTr1_IVXB_PQ.sch"/>
      <include href="include/DTr1_IVXB_REAL.sch"/>
      <include href="include/DTr1_IVXB_TS.sch"/>
      <include href="include/DTr1_list_int.sch"/>
      <include href="include/DTr1_MO.sch"/>
      <include href="include/DTr1_ON.sch"/>
      <include href="include/DTr1_PIVL_TS.sch"/>
      <include href="include/DTr1_PN.sch"/>
      <include href="include/DTr1_PN.CA.sch"/>
      <include href="include/DTr1_PN.NL.sch"/>
      <include href="include/DTr1_PQ.sch"/>
      <include href="include/DTr1_PQR.sch"/>
      <include href="include/DTr1_QTY.sch"/>
      <include href="include/DTr1_REAL.sch"/>
      <include href="include/DTr1_REAL.NONNEG.sch"/>
      <include href="include/DTr1_REAL.POS.sch"/>
      <include href="include/DTr1_RTO.sch"/>
      <include href="include/DTr1_RTO_PQ_PQ.sch"/>
      <include href="include/DTr1_RTO_QTY_QTY.sch"/>
      <include href="include/DTr1_SC.sch"/>
      <include href="include/DTr1_SD.TEXT.sch"/>
      <include href="include/DTr1_SLIST.sch"/>
      <include href="include/DTr1_SLIST_PQ.sch"/>
      <include href="include/DTr1_SLIST_TS.sch"/>
      <include href="include/DTr1_ST.sch"/>
      <include href="include/DTr1_SXCM_INT.sch"/>
      <include href="include/DTr1_SXCM_MO.sch"/>
      <include href="include/DTr1_SXCM_PQ.sch"/>
      <include href="include/DTr1_SXCM_REAL.sch"/>
      <include href="include/DTr1_SXCM_TS.sch"/>
      <include href="include/DTr1_SXPR_TS.sch"/>
      <include href="include/DTr1_TEL.sch"/>
      <include href="include/DTr1_TEL.AT.sch"/>
      <include href="include/DTr1_TEL.CA.EMAIL.sch"/>
      <include href="include/DTr1_TEL.CA.PHONE.sch"/>
      <include href="include/DTr1_TEL.EPSOS.sch"/>
      <include href="include/DTr1_TEL.IPS.sch"/>
      <include href="include/DTr1_TEL.NL.EXTENDED.sch"/>
      <include href="include/DTr1_thumbnail.sch"/>
      <include href="include/DTr1_TN.sch"/>
      <include href="include/DTr1_TS.sch"/>
      <include href="include/DTr1_TS.AT.TZ.sch"/>
      <include href="include/DTr1_TS.AT.VAR.sch"/>
      <include href="include/DTr1_TS.CH.TZ.sch"/>
      <include href="include/DTr1_TS.DATE.sch"/>
      <include href="include/DTr1_TS.DATE.FULL.sch"/>
      <include href="include/DTr1_TS.DATE.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIME.MIN.sch"/>
      <include href="include/DTr1_TS.DATETIMETZ.MIN.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.sch"/>
      <include href="include/DTr1_TS.EPSOS.TZ.OPT.sch"/>
      <include href="include/DTr1_TS.IPS.TZ.sch"/>
      <include href="include/DTr1_URL.sch"/>
      <include href="include/DTr1_URL.NL.EXTENDED.sch"/>
   </pattern>

   <!-- Include the project schematrons related to scenario EntlassungsbriefAerztlich -->

   <!-- CDAEntlassbriefAerztlich -->
   <pattern>
      <title>CDAEntlassbriefAerztlich</title>
      <rule fpi="RUL-EXPEL" context="/">
         <assert role="warning"
                 test="descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.2']]"
                 see="http://elga.art-decor.org/elga-html-20250116T123711/tmp-1.2.40.0.34.11.2-2023-05-08T134415.html">(CDAEntlassbriefAerztlich): Instance is expected to have the following element: descendant-or-self::hl7:ClinicalDocument[hl7:templateId[@root = '1.2.40.0.34.11.1'] and hl7:templateId[@root = '1.2.40.0.34.11.2']]</assert>
      </rule>
   </pattern>
   <include href="include/1.2.40.0.34.11.2-2023-05-08T134415.sch"/>
   <include href="include/1.2.40.0.34.11.2-2023-05-08T134415-closed.sch"/>


   <!-- Create phases for more targeted validation on large instances -->
   <phase id="AllExceptClosed">
      <active pattern="template-1.2.40.0.34.11.2-2023-05-08T134415"/>
      <active pattern="template-1.2.40.0.34.11.1.2.1-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.2.2-2012-07-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.2.3-2015-04-23T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.2.4-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.2.6-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.2.7-2017-02-20T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.3.2-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.3.3-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.3.4-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.1.3.6-2019-12-05T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.1-2015-10-10T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.10-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.11-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.12-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.13-2012-07-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.14-2013-11-07T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.15-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.16-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.17-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.18-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.19-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.2-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.20-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.21-2014-05-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.22-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.23-2013-02-10T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.24-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.25-2015-09-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.26-2017-02-20T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.27-2017-02-20T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.3-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.4-2017-02-20T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.5-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.7-2011-12-19T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.8-2014-05-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.2.9-2015-10-15T000000"/>
      <active pattern="template-1.2.40.0.34.11.2.3.1-2020-08-31T122703"/>
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.1.3.2-2014-05-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.1.3.3-2014-05-14T000000"/>
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
   </phase>
   <phase id="CDAEntlassbriefAerztlich">
      <active pattern="template-1.2.40.0.34.11.2-2023-05-08T134415"/>
   </phase>
   <phase id="CDAEntlassbriefAerztlich-closed">
      <active pattern="template-1.2.40.0.34.11.2-2023-05-08T134415-closed"/>
   </phase>
   <phase id="Brieftext">
      <active pattern="template-1.2.40.0.34.11.1.2.1-2011-12-19T000000"/>
   </phase>
   <phase id="AbschliessendeBemerkung">
      <active pattern="template-1.2.40.0.34.11.1.2.2-2012-07-14T000000"/>
   </phase>
   <phase id="Beilagen">
      <active pattern="template-1.2.40.0.34.11.1.2.3-2015-04-23T000000"/>
   </phase>
   <phase id="Patientenverfuegung">
      <active pattern="template-1.2.40.0.34.11.1.2.4-2011-12-19T000000"/>
   </phase>
   <phase id="VitalparameterEnhanced">
      <active pattern="template-1.2.40.0.34.11.1.2.6-2011-12-19T000000"/>
   </phase>
   <phase id="VitalparameterFull">
      <active pattern="template-1.2.40.0.34.11.1.2.7-2017-02-20T000000"/>
   </phase>
   <phase id="EingebettetesObjektEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.1-2017-05-05T000000"/>
   </phase>
   <phase id="LogoEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.2-2011-12-19T000000"/>
   </phase>
   <phase id="VitalparameterGruppeEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.3-2011-12-19T000000"/>
   </phase>
   <phase id="VitalparameterEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.4-2011-12-19T000000"/>
   </phase>
   <phase id="ProblemEntry">
      <active pattern="template-1.2.40.0.34.11.1.3.6-2019-12-05T000000"/>
   </phase>
   <phase id="Aufnahmegrund">
      <active pattern="template-1.2.40.0.34.11.2.2.1-2015-10-10T000000"/>
   </phase>
   <phase id="TermineKontrollenWiederbestellung">
      <active pattern="template-1.2.40.0.34.11.2.2.10-2011-12-19T000000"/>
   </phase>
   <phase id="Entlassungszustand">
      <active pattern="template-1.2.40.0.34.11.2.2.11-2015-10-15T000000"/>
   </phase>
   <phase id="Aufenthaltszusammenfassung">
      <active pattern="template-1.2.40.0.34.11.2.2.12-2015-10-15T000000"/>
   </phase>
   <phase id="AllergienUnvertraeglichkeitenRisiken">
      <active pattern="template-1.2.40.0.34.11.2.2.13-2012-07-14T000000"/>
   </phase>
   <phase id="Befunde">
      <active pattern="template-1.2.40.0.34.11.2.2.14-2013-11-07T000000"/>
   </phase>
   <phase id="AusstehendeBefunde">
      <active pattern="template-1.2.40.0.34.11.2.2.15-2011-12-19T000000"/>
   </phase>
   <phase id="AuszuegeBefunde">
      <active pattern="template-1.2.40.0.34.11.2.2.16-2015-10-15T000000"/>
   </phase>
   <phase id="BeigelegteBefunde">
      <active pattern="template-1.2.40.0.34.11.2.2.17-2011-12-19T000000"/>
   </phase>
   <phase id="AnamneseAerztlich">
      <active pattern="template-1.2.40.0.34.11.2.2.18-2015-10-15T000000"/>
   </phase>
   <phase id="FruehereErkrankungen">
      <active pattern="template-1.2.40.0.34.11.2.2.19-2015-10-15T000000"/>
   </phase>
   <phase id="EntlassungsdiagnoseEnhanced">
      <active pattern="template-1.2.40.0.34.11.2.2.2-2011-12-19T000000"/>
   </phase>
   <phase id="MedikationEinweisungEnhanced">
      <active pattern="template-1.2.40.0.34.11.2.2.20-2011-12-19T000000"/>
   </phase>
   <phase id="MedikationEinweisungFull">
      <active pattern="template-1.2.40.0.34.11.2.2.21-2014-05-14T000000"/>
   </phase>
   <phase id="MedikationAufenthalt">
      <active pattern="template-1.2.40.0.34.11.2.2.22-2011-12-19T000000"/>
   </phase>
   <phase id="OPBericht">
      <active pattern="template-1.2.40.0.34.11.2.2.23-2013-02-10T000000"/>
   </phase>
   <phase id="EmpfohleneAnordnungenPflege">
      <active pattern="template-1.2.40.0.34.11.2.2.24-2015-10-15T000000"/>
   </phase>
   <phase id="BisherigeMassnahmen">
      <active pattern="template-1.2.40.0.34.11.2.2.25-2015-09-19T000000"/>
   </phase>
   <phase id="Rehabilitationsziele">
      <active pattern="template-1.2.40.0.34.11.2.2.26-2017-02-20T000000"/>
   </phase>
   <phase id="OutcomeMeasurement">
      <active pattern="template-1.2.40.0.34.11.2.2.27-2017-02-20T000000"/>
   </phase>
   <phase id="EntlassungsdiagnoseFull">
      <active pattern="template-1.2.40.0.34.11.2.2.3-2015-10-15T000000"/>
   </phase>
   <phase id="DurchgefuehrteMassnahmen">
      <active pattern="template-1.2.40.0.34.11.2.2.4-2017-02-20T000000"/>
   </phase>
   <phase id="LetzteMedikation">
      <active pattern="template-1.2.40.0.34.11.2.2.5-2011-12-19T000000"/>
   </phase>
   <phase id="EmpfohleneMedikationEnhanced">
      <active pattern="template-1.2.40.0.34.11.2.2.7-2011-12-19T000000"/>
   </phase>
   <phase id="EmpfohleneMedikationFull">
      <active pattern="template-1.2.40.0.34.11.2.2.8-2014-05-14T000000"/>
   </phase>
   <phase id="WeitereMassnahmen">
      <active pattern="template-1.2.40.0.34.11.2.2.9-2015-10-15T000000"/>
   </phase>
   <phase id="EntlassungsdiagnoseEntry">
      <active pattern="template-1.2.40.0.34.11.2.3.1-2020-08-31T122703"/>
   </phase>
   <phase id="MedikationVerordnungEntryemed">
      <active pattern="template-1.2.40.0.34.11.8.1.3.1-2014-09-01T000000"/>
   </phase>
   <phase id="MedikationVerordnungEntryNoDrugTherapy">
      <active pattern="template-1.2.40.0.34.11.8.1.3.2-2014-05-14T000000"/>
   </phase>
   <phase id="MedikationVerordnungEntryPatientNotOnSelfMedication">
      <active pattern="template-1.2.40.0.34.11.8.1.3.3-2014-05-14T000000"/>
   </phase>
   <phase id="MedikationAbgabeEntryemed">
      <active pattern="template-1.2.40.0.34.11.8.2.3.1-2014-09-10T000000"/>
   </phase>

   <!-- Include schematrons from templates with explicit * or ** context (but no representing templates), only those used in scenario template -->

   <!-- Brieftext -->
   <include href="include/1.2.40.0.34.11.1.2.1-2011-12-19T000000.sch"/>
   <!-- AbschliessendeBemerkung -->
   <include href="include/1.2.40.0.34.11.1.2.2-2012-07-14T000000.sch"/>
   <!-- Beilagen -->
   <include href="include/1.2.40.0.34.11.1.2.3-2015-04-23T000000.sch"/>
   <!-- Patientenverfuegung -->
   <include href="include/1.2.40.0.34.11.1.2.4-2011-12-19T000000.sch"/>
   <!-- VitalparameterEnhanced -->
   <include href="include/1.2.40.0.34.11.1.2.6-2011-12-19T000000.sch"/>
   <!-- VitalparameterFull -->
   <include href="include/1.2.40.0.34.11.1.2.7-2017-02-20T000000.sch"/>
   <!-- EingebettetesObjektEntry -->
   <include href="include/1.2.40.0.34.11.1.3.1-2017-05-05T000000.sch"/>
   <!-- LogoEntry -->
   <include href="include/1.2.40.0.34.11.1.3.2-2011-12-19T000000.sch"/>
   <!-- VitalparameterGruppeEntry -->
   <include href="include/1.2.40.0.34.11.1.3.3-2011-12-19T000000.sch"/>
   <!-- VitalparameterEntry -->
   <include href="include/1.2.40.0.34.11.1.3.4-2011-12-19T000000.sch"/>
   <!-- ProblemEntry -->
   <include href="include/1.2.40.0.34.11.1.3.6-2019-12-05T000000.sch"/>
   <!-- Aufnahmegrund -->
   <include href="include/1.2.40.0.34.11.2.2.1-2015-10-10T000000.sch"/>
   <!-- TermineKontrollenWiederbestellung -->
   <include href="include/1.2.40.0.34.11.2.2.10-2011-12-19T000000.sch"/>
   <!-- Entlassungszustand -->
   <include href="include/1.2.40.0.34.11.2.2.11-2015-10-15T000000.sch"/>
   <!-- Aufenthaltszusammenfassung -->
   <include href="include/1.2.40.0.34.11.2.2.12-2015-10-15T000000.sch"/>
   <!-- AllergienUnvertraeglichkeitenRisiken -->
   <include href="include/1.2.40.0.34.11.2.2.13-2012-07-14T000000.sch"/>
   <!-- Befunde -->
   <include href="include/1.2.40.0.34.11.2.2.14-2013-11-07T000000.sch"/>
   <!-- AusstehendeBefunde -->
   <include href="include/1.2.40.0.34.11.2.2.15-2011-12-19T000000.sch"/>
   <!-- AuszuegeBefunde -->
   <include href="include/1.2.40.0.34.11.2.2.16-2015-10-15T000000.sch"/>
   <!-- BeigelegteBefunde -->
   <include href="include/1.2.40.0.34.11.2.2.17-2011-12-19T000000.sch"/>
   <!-- AnamneseAerztlich -->
   <include href="include/1.2.40.0.34.11.2.2.18-2015-10-15T000000.sch"/>
   <!-- FruehereErkrankungen -->
   <include href="include/1.2.40.0.34.11.2.2.19-2015-10-15T000000.sch"/>
   <!-- EntlassungsdiagnoseEnhanced -->
   <include href="include/1.2.40.0.34.11.2.2.2-2011-12-19T000000.sch"/>
   <!-- MedikationEinweisungEnhanced -->
   <include href="include/1.2.40.0.34.11.2.2.20-2011-12-19T000000.sch"/>
   <!-- MedikationEinweisungFull -->
   <include href="include/1.2.40.0.34.11.2.2.21-2014-05-14T000000.sch"/>
   <!-- MedikationAufenthalt -->
   <include href="include/1.2.40.0.34.11.2.2.22-2011-12-19T000000.sch"/>
   <!-- OPBericht -->
   <include href="include/1.2.40.0.34.11.2.2.23-2013-02-10T000000.sch"/>
   <!-- EmpfohleneAnordnungenPflege -->
   <include href="include/1.2.40.0.34.11.2.2.24-2015-10-15T000000.sch"/>
   <!-- BisherigeMassnahmen -->
   <include href="include/1.2.40.0.34.11.2.2.25-2015-09-19T000000.sch"/>
   <!-- Rehabilitationsziele -->
   <include href="include/1.2.40.0.34.11.2.2.26-2017-02-20T000000.sch"/>
   <!-- OutcomeMeasurement -->
   <include href="include/1.2.40.0.34.11.2.2.27-2017-02-20T000000.sch"/>
   <!-- EntlassungsdiagnoseFull -->
   <include href="include/1.2.40.0.34.11.2.2.3-2015-10-15T000000.sch"/>
   <!-- DurchgefuehrteMassnahmen -->
   <include href="include/1.2.40.0.34.11.2.2.4-2017-02-20T000000.sch"/>
   <!-- LetzteMedikation -->
   <include href="include/1.2.40.0.34.11.2.2.5-2011-12-19T000000.sch"/>
   <!-- EmpfohleneMedikationEnhanced -->
   <include href="include/1.2.40.0.34.11.2.2.7-2011-12-19T000000.sch"/>
   <!-- EmpfohleneMedikationFull -->
   <include href="include/1.2.40.0.34.11.2.2.8-2014-05-14T000000.sch"/>
   <!-- WeitereMassnahmen -->
   <include href="include/1.2.40.0.34.11.2.2.9-2015-10-15T000000.sch"/>
   <!-- EntlassungsdiagnoseEntry -->
   <include href="include/1.2.40.0.34.11.2.3.1-2020-08-31T122703.sch"/>
   <!-- MedikationVerordnungEntryemed -->
   <include href="include/1.2.40.0.34.11.8.1.3.1-2014-09-01T000000.sch"/>
   <!-- MedikationVerordnungEntryNoDrugTherapy -->
   <include href="include/1.2.40.0.34.11.8.1.3.2-2014-05-14T000000.sch"/>
   <!-- MedikationVerordnungEntryPatientNotOnSelfMedication -->
   <include href="include/1.2.40.0.34.11.8.1.3.3-2014-05-14T000000.sch"/>
   <!-- MedikationAbgabeEntryemed -->
   <include href="include/1.2.40.0.34.11.8.2.3.1-2014-09-10T000000.sch"/>

</schema>
<!-- versionLabel="11.6.0+20250116-2.6.6" -->
